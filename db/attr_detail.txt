
--
-- Table structure for table `ec_attribute_detail`
--

DROP TABLE IF EXISTS `ec_attribute_detail`;
CREATE TABLE IF NOT EXISTS `ec_attribute_detail` (
  `_ID` int(11) NOT NULL AUTO_INCREMENT,
  `_ProID` int(11) DEFAULT NULL,
  `_AttID` int(11) DEFAULT NULL,
  `_Attinfo` json DEFAULT NULL,
  PRIMARY KEY (`_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ec_attribute_detail`
--

INSERT INTO `ec_attribute_detail` (`_ID`, `_ProID`, `_AttID`, `_Attinfo`) VALUES
(1, 6, 1, '[{\"id\": \"S\", \"price\": \"111\", \"quantity\": \"11\"}, {\"id\": \"M\", \"price\": \"220\", \"quantity\": \"22\"}, {\"id\": \"L\", \"price\": \"300\", \"quantity\": \"310\"}, {\"id\": \"XXL\", \"price\": \"40\", \"quantity\": \"4111\"}]'),
(2, 2, 13, '[{\"id\": \"Red\", \"price\": \"10\", \"quantity\": \"10\"}, {\"id\": \" Green\", \"price\": \"20\", \"quantity\": \"20\"}, {\"id\": \" Blue\", \"price\": \"30\", \"quantity\": \"30\"}, {\"id\": \" Yellow\", \"price\": \"20\", \"quantity\": \"20\"}]'),
(3, 2, 1, '[{\"id\": \"S\", \"price\": \"11\", \"quantity\": \"11\"}, {\"id\": \"M\", \"price\": \"22\", \"quantity\": \"22\"}, {\"id\": \"L\", \"price\": \"33\", \"quantity\": \"33\"}, {\"id\": \"XXL\", \"price\": \"44\", \"quantity\": \"44\"}]'),
(4, 6, 13, '[{\"id\": \"Red\", \"price\": \"100\", \"quantity\": \"200\"}, {\"id\": \" Green\", \"price\": \"\", \"quantity\": \"\"}, {\"id\": \" Blue\", \"price\": \"\", \"quantity\": \"\"}, {\"id\": \" Yellow\", \"price\": \"\", \"quantity\": \"\"}]');
COMMIT;

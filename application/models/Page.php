<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Page extends Eloquent {

    protected $table = "ec_pages"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";
}
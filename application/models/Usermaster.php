<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Usermaster extends Eloquent {

    protected $table = "ec_usermaster"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";
}
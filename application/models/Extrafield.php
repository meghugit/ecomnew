<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Extrafield extends Eloquent {

    protected $table = "ec_extra_field"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";


}
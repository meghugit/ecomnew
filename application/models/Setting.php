<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Setting extends Eloquent {

    protected $table = "ec_settings"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";


}
<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Orderdetail extends Eloquent {
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

    protected $table = "ec_order_detail"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";


    public function users(){
        return $this->belongsTo('Users','_UserID');
    }

    public function payment(){
        return $this->belongsTo('Payments','_Getwayid');
    }

    public function product(){ 
        return $this->belongsTo('Products', '_ProductID');
    }

 
}
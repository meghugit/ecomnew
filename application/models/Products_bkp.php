<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Products extends Eloquent {
    use \Staudenmeir\EloquentJsonRelations\HasJsonRelationships;

    protected $table = "ec_product"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";
    protected $casts = [
            '_Specification' => 'array',
            '_Adddetails' => 'array',
            '_Image' => 'array',
        ];

    public function att_detail_func(){
        return $this->belongsTo('Attdetail','_ID','_ProID');
    }

 	public function categories(){
    	return $this->belongsTo('Categories','_CatID', '_ID');
    }

    public function subcategory(){
       return $this->belongsTo('Categories','_SubcatID', '_ID');
    }

    public function orders() {
        return $this->hasManyJson(Order::class, '_Productdetail[]->id');
    }

    public function reviews(){
        return $this->hasMany('Reviews','_ProductID');  
    }

    public function wishlists(){
       return $this->hasMany('Wishlists','_Productid');
    }

    
}
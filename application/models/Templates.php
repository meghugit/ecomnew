<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Templates extends Eloquent {

    protected $table = "ec_template"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";
     protected $casts = [
            '_Type' => 'array',
    ];

}
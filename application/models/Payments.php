<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Payments extends Eloquent {

    protected $table = "ec_payment"; // table name
    public $timestamps = false;
    
    protected $primaryKey = "_ID";

    protected $casts = [
        '_Details' => 'array'
    ];
}
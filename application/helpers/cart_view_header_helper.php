<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

// ------------------------------------------------------------------------

function cart_view_header(){

		$CI =& get_instance();
        //$loguser_id=$CI->encryption->decrypt(get_cookie('userid'));
        $loguser_id= null !== get_cookie('userid') ? $CI->encryption->decrypt(get_cookie('userid')) : get_cookie('userid_new');

       $CI =& get_instance();
        $loguser_id= null !== get_cookie('userid') ? $CI->encryption->decrypt(get_cookie('userid')) : get_cookie('userid_new');
        $cart_data_new = Carts1::with('product')
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();

        $data = '';
          $total = 0;
          for($i = 0 ;$i < count($cart_data_new) ; $i++){
          $data .= '<li class="item product product-item">'
                                        .'<div class="product">'
                                          .'<a class="product-item-photo" href="#" title="'.$cart_data_new[$i]["product"]["_Name"].'">'
                                            .'<span class="product-image-container">'
                                        .'<span class="product-image-wrapper">'
                                          .'<img class="product-image-photo" src="'.base_url().'assets/uploads/product/'.$cart_data_new[$i]["product"]["_Image"][0]["thumb"].'" alt="'.$cart_data_new[$i]["product"]["_Name"].'">'
                                        .'</span>'
                                          .'</span>'
                                      .'</a>'
                                      .'<div class="product-item-details">'
                                          .'<div class="product-item-name">'
                                            .'<a href="#">'.$cart_data_new[$i]["product"]["_Name"].'</a><b style="color:#ff8400;">  ('.$cart_data_new[$i]["_Color"].')</b>'
                                          .'</div>'
                                          .'<div class="product-item-qty">'
                                            .'<label class="label">Qty</label>'
                                            .'<input class="item-qty cart-item-qty" maxlength="12" value="'.$cart_data_new[$i]["_Quantity"].'">'
                                            .'<button class="update-cart-item" style="display: none" title="Update">'
                                                .'<span>Update</span>'
                                            .'</button>'
                                          .'</div>'
                                          .'<div class="product-item-pricing">'
                                            .'<div class="price-container">'
                                                .'<span class="price-wrapper">'
                                              .'<span class="price-excluding-tax">'
                                                .'<span class="minicart-price">'
                                                  .'<span class="price">'.$cart_data_new[$i]["_Subtotal"].'</span> </span>'
                                                  .'</span>'
                                                .'</span>'
                                            .'</div>'
                                            .'<div class="product actions">'
                                                .'<div class="secondary">'
                                                  .'<a href="#" class="action delete" title="Remove item">'
                                                    .'<span>Delete</span>'
                                                .'</a>'
                                              .'</div>'
                                              .'<div class="primary">'
                                                .'<a class="action edit" href="#" title="Edit item">'
                                                    .'<span>Edit</span>'
                                                .'</a>'
                                              .'</div>'
                                          .'</div>'
                                        .'</div>'
                                    .'</div>'
                                .'</div>'
                            .'</li>';
                            $total += $cart_data_new[$i]["_Subtotal"];
        }
          $data .= '</ol>';
    $data .= '<div class="subtotal">
          <span class="label">
    <span>Subtotal</span>
        </span>
        <div class="amount price-container">
          <span class="price-wrapper"><span class="price">$'.$total.'</span></span>
        </div>
      </div>';
        
        
      return $data;

    }

function cart_view_count(){

		$CI =& get_instance();
        //$loguser_id=$CI->encryption->decrypt(get_cookie('userid'));
        $loguser_id= null !== get_cookie('userid') ? $CI->encryption->decrypt(get_cookie('userid')) : get_cookie('userid_new');


    $CI =& get_instance();
      
        $loguser_id= null !== get_cookie('userid') ? $CI->encryption->decrypt(get_cookie('userid')) : get_cookie('userid_new');
        $cart_data_new = Carts1::with('product')
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();
        return count($cart_data_new);
}

function product_view($id){
    $CI =& get_instance();
        

          $cat_data = Categories::where('_Main_id',$id)
                                ->get()
                                ->toArray();

          $data = '';
          for($i = 0 ;$i < count($cat_data) ; $i++){
          $data .= '<div class="col">
                          <a class="category-image" href="'.base_url().'productlist/'.$cat_data[$i]["_ID"].'"><img src="'.base_url().'assets/uploads/subcategory/'.$cat_data[$i]["_Image"].'" alt /></a>
                          <div class="category-title"><a href="'.base_url().'productlist/'.$cat_data[$i]["_ID"].'">'.$cat_data[$i]["_Name"].'</a></div>
                        </div>'; 
        }


        return $data;
}


?>
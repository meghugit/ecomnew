{% extends 'front/app/index.php' %}

{% block content %} 
<main class="page-main">
				<div class="block">
					<div class="container">
						<ul class="breadcrumbs">
							<li><a href="index.html"><i class="icon icon-home"></i></a></li>
							<li>/<span>Order Details</span></li>
						</ul>
					</div>
					
				</div>

				<!-- <div class="block"> -->
					<div class="row">
				 <div class="col-xs-6 col-sm-3">  
					
				                  <br>
				                  <b>Order ID:</b>{{orprodetail._Orderid}}<br>
				                  <b>Payment Status:</b> {% if (orprodetail._Paymentstatus)==0 %}Pending
				                  {% elseif(orprodetail._Paymentstatus)==1 %}Paid
				                  {% else %}Refund
				                  {% endif %}

				                  <br>
				                  <b>Order Status:</b> {% if (orprodetail._Orderstatus)==0 %}Pending
				                  {% elseif(orprodetail._Orderstatus)==1 %}Approved
				                  {% elseif(orprodetail._Orderstatus)==2 %}Shipping
				                  {% elseif(orprodetail._Orderstatus)==3 %}Deliverd
				                  {% else %}Cancelled
				                  {% endif %}<br>
				                  <b>Order Date:</b> {{orprodetail._Orderdate|date('d/m/Y')}}
				                 </div> 
				 </div> 
				</div>
			<!-- </div> -->
			
				<div class="block">
					<div class="container">
						<div class="cart-table">



			<div class="block">
	          <div class="container">
	            <div class="row">
	              <div class="col-md-12 col-sm-12">

	                <div class="panel panel-default">
	                  <div class="panel-heading">
	                  <ul class="simple-list">
	                          <br>
	                         <li> <b>Order ID:</b>{{orprodetail[0]._Orderid}}<br></li>
	                          <li><b>Payment Status:</b> {% if (orprodetail[0]._Paymentstatus)==0 %}Pending
	                          {% elseif(orprodetail[0]._Paymentstatus)==1 %}Paid
	                          {% else %}Refund
	                          {% endif %}</li>

	                         
	                          <li><b>Order Status:</b> {% if (orprodetail[0]._Orderstatus)==0 %}Pending
	                          {% elseif(orprodetail[0]._Orderstatus)==1 %}Approved
	                          {% elseif(orprodetail[0]._Orderstatus)==2 %}Shipping
	                          {% elseif(orprodetail[0]._Orderstatus)==3 %}Deliverd
	                          {% else %}Cancelled
	                          {% endif %}<br></li>
	                          <li><b>Order Date:</b> {{orprodetail[0]._Orderdate|date('d/m/Y')}}
	                        </li>
		                    </ul>
		                </div>

		             <div class="panel-body">
	                  <div class="cart-table">
							<div class="table-header">
								<div class="photo">
									Product Image
								</div>
								<div class="name">
									 Name
								</div>
								<div class="qty">
									 Quantity
								</div>
								<div class="price">
									Price
								</div>
								<div class="subtotal">
									Total
								</div>
							</div>
							{% if count(orprodetail) > 0 %}
							{% set total = 0 %}
							{% set subtotal = 0 %}
							{% set discount = 10.00 %}
							{% for rowdata in orprodetail['product'] %}
							{% for rowdata in orprodetail[0].orderdetail %}

							<div class="table-row">
							
								<div class="photo">
									<a href="">
										<img src="{{base_url()}}assets/uploads/product/{{rowdata._Image[0]['thumb']}}" alt="">
									</a>
								</div>
								<div class="name">
									<a href="{{base_url('product_detail/')~rowdata._ID}}">{{rowdata._Name}}</a>
								</div>
							
								<div class="price">
									{{rowdata['pivot'].quantity}}
								</div>
							
								<div class="price">
									${{rowdata._Sellprice}}
								</div>
								
								<div class="subtotal">
									{% set total = rowdata['pivot'].quantity * rowdata._Sellprice %}
									{% set subtotal = total + subtotal %}
									{{total}}

										<img src="{{base_url()}}assets/uploads/product/{{rowdata['product']._Image[0]['thumb']}}" alt="">
									</a>
								</div>
								<div class="name">
									<a href="{{base_url('product_detail/')~rowdata['product']._ID}}">{{rowdata['product']._Name}}</a>
								</div>
							
								<div class="price">
									{{rowdata._Quantity}}
								</div>
							
								<div class="price">
									${{rowdata._Unitprice}}
								</div>
								
								<div class="subtotal">
									${{rowdata._Subtotal}}
								</div>
							</div>
							{% endfor %}
					 {% endif %}
							
						</div>

						        {% set i = 0 %}
              {% set discount = 0 %}
              {% set couponname = '' %}
              {% set cat_yes = 0 %}
              {% set ccat = coupon_data[i]._CatID %}
 
                {% for cdata in orprodetail['product'] %}
                  {% if cdata['_CatID'] == ccat %}
                    {% set scct = cdata['_SubcatID'] %}

                  
                    
                    {% for scct in coupon_data[i]['_Subcatid'] %}
                      {% set cat_yes = cat_yes + 1 %}
                    {% endfor %}
   
                    {% if cat_yes == count(coupon_data[i]['_Subcatid']) %}
                        {% set discount = subtotal - coupon_data[i]['_Amount']  %}
                        {% set couponname = coupon_data[i]['_Code'] %}
                   
                       
                    {% endif %}

                  {% endif %}
                {% set i = i + 1 %}
                {% endfor %}
						<div class="row">

						</div>  
					
	              	</div>
	              </div>
	          </div>
	      </div>
                  
			
				<div class="block">
					<div class="container">
						
								<div class="row">
							<div class="col-md-3 total-wrapper">
								<table class="total-price">
									<tr>
										<td>Subtotal</td>
										<td>${{subtotal}}</td>
									</tr>
									<tr>
										<td>Discount</td>
										 <td>{{ discount }}<br>({{couponname}})</td>
										<td>${{orprodetail[0]._Subtotal}}</td>
									</tr>
									<tr>
										{% if orprodetail[0]._Discount != 0 %}
					                    <td>Discount</td>
					                     <td><br>
					                     ({{orprodetail[0].coupon._Code}})
					                 		({{orprodetail[0]._Discount}})</td>
					                    {% endif %}
									</tr>
									 {% set grandtotal = subtotal - discount %}
									<tr class="total">
										<td>Grand Total</td>
										<td>${{grandtotal}}</td>
										<td>${{orprodetail[0]._Grandtotal}}</td>
									</tr>
								</table>
								
							</div>

						</div>
					</div>
				</div>
			</main>
{% endblock %}
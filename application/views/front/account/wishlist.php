{% extends 'front/app/index.php' %}

{% block content %} 
<main class="page-main">
				<div class="block">
					<div class="container">
						<ul class="breadcrumbs">
							<li><a href="index.html"><i class="icon icon-home"></i></a></li>
							<li>/<span>My Wishlist</span></li>
						</ul>
					</div>
				</div>
				<div class="block">
					<div class="container">
						<div class="cart-table wishlist">
							<div class="table-header">
								<div class="photo">
									Product Image
								</div>
								<div class="name">
									Product Name
								</div>
								<div class="price">
									Unit Price
								</div>
								<div class="remove">
									<span class="hidden-sm hidden-xs">Remove</span>
								</div>
							</div>
							{% if count(wishdata) > 0 %}
							{% set total = 0 %}
							{% set avgrate = 0 %}
									
	                    				{% for wdata in wishdata %}
	                    				{% set row =  count(wdata['products']['reviews']) %}
							<div class="table-row">
								<div class="photo">
									<a href="{{base_url('product_detail/')~wdata['products']._ID}}"><img src="{{base_url()}}assets/uploads/product/{{wdata['products']._Image[0]['thumb'] }}" alt=""></a>
								</div>
								<div class="name">
									<a href="{{base_url('product_detail/')~wdata['products']._ID}}">{{wdata['products']._Name}}</a>
									{% for rdata in wdata['products']['reviews'] %}
							 		{% set total = total + rdata._Rate %}
									{% set avgrate = total /row %}
									{% endfor %}
									{% set total = 0 %}
									<div class="rate">
									<span class="badge badge-success">{{avgrate}} <i class="fa fa-star" aria-hidden="true"></i></span>
									</div>
								</div>
								<div class="price">
									${{wdata['products']._Sellprice}}
								</div>
								<div class="remove">
									<a href="{{base_url('remove_wishlist/'~wdata._ID)}}"><span>Remove</span><i class="icon icon-close-2"></i></a>
								</div>
							</div>
							{% endfor %}
							{% endif %}
							
							<div class="table-footer">
								<a class="btn btn-alt" href="{{base_url('productlist')}}">CONTINUE SHOPPING</a>
							</div>
						</div>
					</div>
				</div>
			<!-- 	<div class="block">
				<div class="container">
					<div class="row">
			
					{% if count(wishdata) > 0 %}
					{% set total = 0 %}
					{% set avgrate = 0 %}
							
			                    				{% for wdata in wishdata %}
			                    				{% set row =  count(wdata['products']['reviews']) %}
						
						<div class="box-left-icon-bg">
									<div class="box-icon"><img src="{{base_url()}}assets/uploads/product/{{wdata['products']._Image[0]['thumb'] }}" height="100px" width="100px" alt=""></div>
									<div class="box-text">
										<div class="title">{{wdata['products']._Name}}</div>
										<i class="fa fa-inr">{{wdata['products']._Price}}</i>
										{% for rdata in wdata['products']['reviews'] %}
								 		{% set total = total + rdata._Rate %}
										{% set avgrate = total /row %}
										{% endfor %}
										{% set total = 0 %}
										<div class="rate">
										<span class="badge badge-success">{{avgrate}} <i class="fa fa-star" aria-hidden="true"></i></span>
										</div>
									<div>
									<a href="" class="btn">Buy Now</a>
							</div>
			
									</div>
								</div>
							{% endfor %}
							{% endif %}
						
						<hr>
				</div>
				</div>
			</div> -->
			</main>
{% endblock %}
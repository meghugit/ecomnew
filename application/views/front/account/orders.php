{% extends 'front/app/index.php' %}

{% block content %} 
<main class="page-main">
	<div class="block">
		<div class="container">
			<ul class="breadcrumbs">
				<li><a href="index.html"><i class="icon icon-home"></i></a></li>
				<li>/<span>My Orders</span></li>
			</ul>
		</div>
	</div>
	<div class="block">
		<div class="container">

			<div class="form-card">
				<div class="panel panel-default">
					<div class="panel-heading"><h4>Basic Detail</h4></div>
					<div class="panel-body">
			
				<form class="account-create" id="order_form" name="order_form" method="post">
					<table>
					 {% if count(orderdata) > 0 %}
					 {% for odata in orderdata['order'] %}
					 <tr>
					 	<td><label>Order ID</label></td>
					 	<td>{{odata._Orderid}}</td>
					</tr>
					 <tr>
					 	<td><label>Total</label></td>
					 	<td>{{odata._Total}}</td>
					</tr>
					 <tr>
					 	<td><label>Order Date</label></td>
					 	<td>{{odata._Orderdate|date('d-m-Y')}}</td>
					</tr>
					 <tr>
					 	<td><label>Payment Status</label></td>
					 	<td>{% if(odata._Paymentstatus)==0 %}Pending
							{% elseif(odata._Paymentstatus)==1 %}
							paid
							{% else %}refund
							{% endif %}
					 	</td>
					</tr>
					 <tr>
					 	<td><label>Order Status</label></td>
					 	<td>{% if(odata._Orderstatus)==0 %}<span class="badge badge-warning">Pending</span>
							{% elseif(odata._Orderstatus)==1 %}
							<span class="badge badge-success">Approved</span>
							{% elseif(odata._Orderstatus)==2 %}
							<span class="badge badge-warning">Shipped</span>
							{% elseif(odata._Orderstatus)==3 %}
							<span class="badge badge-danger">Delivered</span>
							{% else %}<span class="badge badge-info">Cancelled</span>
							{% endif %}
						</td>
					</tr>
					<tr>
						<td>
						<a href="{{base_url('orderdetail/')~ odata._ID}}" class="btn btn-success" >Order Detail</a>
					</td>
					</tr>

					 {% endfor %}
					 {% endif %}
				
				</table>
				</form>
                 </div>
             </div>


			</div>
		</div>
			</div>
	

			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">ORDER ID</th>
									<th scope="col">ORDER DATE </th>
									<th scope="col">ORDER TOTAL</th>
									<th scope="col">ORDER STATUS</th>
									<th scope="col">VIEW ORDER DEETAIL</th>
								</tr>
							</thead>
							<tbody>
								{% set i = 1 %}
								{% if count(orderdata) > 0 %}
					 			{% for odata in orderdata['order'] %}
								<tr>
									<td>{{i}}</td>
									<td>{{odata._Orderid}}</td>
									<td>{{odata._Orderdate|date('d-m-Y')}}</td>
									<td>${{odata._Grandtotal}}</td>
									<td>
										{% if(odata._Orderstatus)==0 %}
										<span class="badge badge-warning">Pending</span>
										{% elseif(odata._Orderstatus)==1 %}
										<span class="badge badge-success">Approved</span>
										{% elseif(odata._Orderstatus)==2 %}
										<span class="badge badge-warning">Shipped</span>
										{% elseif(odata._Orderstatus)==3 %}
										<span class="badge badge-danger">Delivered</span>
										{% else %}<span class="badge badge-info">Cancelled</span>
										{% endif %}
									</td>
									<td>
										<a href="{{base_url('orderdetail/')~ odata._ID}}" class="btn btn-success" >Order Detail</a>
									</td>
								</tr>
								{% set i = i+1 %}
								{% endfor %}
					 			{% endif %}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

</main>
{% endblock %}
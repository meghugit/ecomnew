{% extends 'front/app/index.php' %}

{% block content %} 
  <!-- Loader -->
  <div id="loader-wrapper" class="off">
    <div class="cube-wrapper">
      <div class="cube-folding">
        <span class="leaf1"></span>
        <span class="leaf2"></span>
        <span class="leaf3"></span>
        <span class="leaf4"></span>
      </div>
    </div>
  </div>
  <!-- /Loader -->
  <div class="fixed-btns">
    <!-- Back To Top -->
    <a href="#" class="top-fixed-btn back-to-top"><i class="icon icon-arrow-up"></i></a>
    <!-- /Back To Top -->
  </div>

    <!-- Page Content -->
      <main class="page-main">
        <div class="block fullwidth full-nopad bottom-space">
          <div class="container">
            <!-- Main Slider -->
            <div class="mainSlider" data-thumb="true" data-thumb-width="230" data-thumb-height="100">
              <div class="sliderLoader">Loading...</div>
              <!-- Slider main container -->
              <div class="swiper-container">
                <div class="swiper-wrapper">
                  <!-- Slides -->
                 <!--  <div class="swiper-slide" data-thumb="{{ constant('fronttheme') }}images/slider/slide-02-thumb.png" data-href="http://google.com" data-target="_blank">
                   _blank or _self ( _self is default )
                   <div class="wrapper">
                     <figure><img src="{{ constant('fronttheme') }}images/slider/slide-02.jpg" alt=""></figure>
                     <div class="text2-1 animate" data-animate="flipInY" data-delay="0"> Seikō </div>
                     <div class="text2-2 animate" data-animate="bounceIn" data-delay="500"> Season sale </div>
                     <div class="text2-3 animate" data-animate="bounceIn" data-delay="1000"> popular brands </div>
                     <div class="text2-4 animate" data-animate="rubberBand" data-delay="1500"> 70% </div>
                     <div class="text2-5 animate" data-animate="hinge" data-delay="2000"> OFF </div>
                   </div>
                 </div> -->
                  <!-- <div class="swiper-slide" data-thumb="{{ constant('fronttheme') }}images/slider/slide-01-thumb.png">
                    <div class="wrapper">
                      <figure><img src="{{ constant('fronttheme') }}images/slider/slide-01.jpg" alt=""></figure>
                      <div class="caption animate" data-animate="fadeIn">
                        <div class="text1 animate" data-animate="flipInY" data-delay="0"> Seikō </div>
                        <div class="text2 animate" data-animate="bounceInLeft" data-delay="500"> <strong>New</strong> collection </div>
                        <div class="text3 animate" data-animate="bounceInLeft" data-delay="1500"> WOMEN'S <strong>FASHION</strong> </div>
                        <div class="animate" data-animate="fadeIn" data-delay="2000">
                          coolbutton
                          <a href="#" class="cool-btn" style="-webkit-clip-path: url(#coolButton); clip-path: url(#coolButton);"> <span>MORE</span> </a>
                          <svg class="clip-svg">
                            <defs>
                              <clipPath id="coolButton" clipPathUnits="objectBoundingBox">
                                <polygon points="0 .18, .99 .15, .91 .85, .07 .8" />
                              </clipPath>
                            </defs>
                          </svg>
                          // coolbutton
                        </div>
                      </div>
                    </div>
                  </div> -->
                  {% if count(extdata) > 0 %}
                  {% for edetail in extdata %}
                  <div class="swiper-slide" data-thumb="{{ base_url() }}assets/uploads/setting/{{edetail._Image}}">
                    <div class="wrapper">
                      <figure><img src="{{ base_url() }}assets/uploads/setting/{{edetail._Image}}" alt=""></figure>
                     <div class="text4-1 animate" data-animate="bounceInDown" data-delay="0" >{{edetail._Title1}} </div>
                     <div class="text2-2 animate" data-animate="rubberBand" data-delay="500" >{{edetail._Title2}} </div>
                     <!--style="color: #333745"   style="color: #e94e04"<div class="text3-3 animate" data-animate="bounceInDown" data-delay="1000"> And </div>
                     <div class="text3-4 animate" data-animate="bounceIn" data-delay="1500"> Urban </div>
                     <div class="text3-5 animate" data-animate="bounceIn" data-delay="2000"> Subcultures </div>
                     <a href="#" class="text3-6 animate" data-animate="rubberBand" data-delay="2500"> SHOP NOW </a> -->
                    </div>
                  </div>
                  {% endfor %}
                  {% endif %}
                 <!--  <div class="swiper-slide" data-thumb="{{ constant('fronttheme') }}images/slider/slide-04-thumb.png">
                   <div class="wrapper">
                     <figure><img src="{{ constant('fronttheme') }}images/slider/slide-04.jpg" alt=""></figure>
                     <div class="text4-1 animate" data-animate="bounceInLeft" data-delay="0">Summer</div>
                     <div class="text4-2 animate" data-animate="bounceInDown" data-delay="500">very soon</div>
                     <div class="text4-3 animate" data-animate="bounceInUp" data-delay="1000">Things to buy a swimsuit at a discount</div>
                     <a href="#" class="text4-4 animate" data-animate="bounceInLeft" data-delay="1500">SHOP NOW</a>
                   </div>
                 </div> -->
                </div>
                <!-- pagination -->
                <div class="swiper-pagination"></div>
                <!-- pagination thumbs -->
                <div class="swiper-pagination-thumbs">
                  <div class="thumbs-wrapper">
                    <div class="thumbs"></div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /Main Slider -->
          </div>
        </div>
        <div class="block">
          <div class="container">
            <!-- Wellcome text -->
            <div class="text-center bottom-space">
              <h1 class="size-lg no-padding">WELCOME TO <span class="logo-font custom-color">Seikō</span> STORE</h1>
              <div class="line-divider"></div>
              <p class="custom-color-alt">Lorem ipsum dolor sit amet, ex eam mundi populo accusamus, aliquam quaestio petentium te cum.
                <br> Vim ei oblique tacimates, usu cu iudico graeco. Graeci eripuit inimicus vel eu, eu mel unum laoreet splendide, cu integre apeirian has.
              </p>
            </div>
            <!-- /Wellcome text -->
          </div>
        </div>
        <div class="block">
          <div class="container">
            <div class="row">
              <div class="col-sm-4">
                <div class="box style2 bgcolor1 text-center">
                  <div class="box-icon"><i class="icon icon-truck"></i></div>
                  <h3 class="box-title">FREE SHIPPING</h3>
                  <div class="box-text">Free shipping on all orders over $199</div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="box style2 bgcolor2 text-center">
                  <div class="box-icon"><i class="icon icon-dollar"></i></div>
                  <h3 class="box-title">MONEY BACK</h3>
                  <div class="box-text">100% money back guarantee</div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="box style2 bgcolor3 text-center">
                  <div class="box-icon"><i class="icon icon-help"></i></div>
                  <h3 class="box-title">ONLINE SUPPORT</h3>
                  <div class="box-text">Service support fast 24/7</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        
        <div class="block">
          <div class="container">
            <ul class="filters filters-product style2">
              <li><a href="#" class="filter-label active">All<span class="count"></span></a></li>
        <li><a href="#" class="filter-label" data-filter=".new">New<span class="count"></span></a></li>
        <li><a href="#" class="filter-label" data-filter=".sale">Sale<span class="count"></span></a></li>
       <!--  <li><a href="#" class="filter-label" data-filter=".popular">Popular<span class="count"></span></a></li> -->
            </ul>
            <div class="products-grid-wrapper isotope-wrapper">
              <div class="products-grid isotope four-in-row product-variant-5">

      {% if  products|length > 0 %}
         {% for product in products %} 

          <div class="product-item large {{ product.class }}">
            <div class="product-item-inside">
              <div class="product-item-info">
                <div class="product-item-photo">
                  {% if product['product']['_Image'] |length == 1 %}
                  <div class="product-item-gallery">
                    <div class="product-item-gallery-main">
                      <a href="{{base_url('product_detail/')~product['product']._ID}}"><img class="product-image-photo" src="assets/uploads/product/{{ product['product']['_Image'][0]['cat'] }}" alt="product" height="200"></a>
                    </div>
                  </div>
                  {% endif %}
                  {% if product['product']['_Image'] |length> 1 %}
                    <div class="carousel-inside slide" data-ride="carousel">
                      <div class="carousel-inner" role="listbox">
                        {% set i = 0%}
                        {% for image in product['product']['_Image'] %}

                        {% if(i == 0) %}
                          <div class="item active">
                            <a href="{{base_url('product_detail/')~product['product']._ID}}">
                              <img class="product-image-photo" src="assets/uploads/product/{{ image['cat'] }}" alt="" />
                            </a>
                          </div>
                        {% else %}
                          <div class="item">
                            <a href="{{base_url('product_detail/')~product._ID}}">
                              <img class="product-image-photo" src="assets/uploads/product/{{ image['cat'] }}" alt="" />
                            </a>
                          </div>
                        {% endif %}
                        {% set i = i + 1 %}
                        {% endfor %}
                      </div>
                      <a class="carousel-control next"></a>
                      <a class="carousel-control prev"></a>
                    </div>
                  {% endif %}

                  <a href="{{base_url('quick_view/')~product['product']._ID}}" title="Quick View" class="quick-view-link quick-view-btn">
                    <i class="icon icon-eye"></i>
                    <span>Quick View</span>
                  </a>
                  {% if get_cookie('userid') == '' %}

                  <a data-toggle="modal" data-target="#modal3" title="Add to Wishlist" class="no_wishlist add"> <i class="icon icon-heart"></i><span>Add to Wishlist</span> </a>
                  
                  {% else %}
              

                  {% if product['product'].wishlists |length > 0 %}
                    <a onclick ="ecommerce.notifyWithtEle('product is already exist in wishlist' , 'success' ,'topRight', 2000);"  title="In Wishlist" class="no_wishlist" style="color:#7e57c2 !important;"> <i class="icon icon-heart"></i><span>In Wishlist</span> </a>
                  {% else %}
                    <a href="{{base_url('addwishlist/')~product['product']._ID}}" title="Add to Wishlist" class="no_wishlist add" data-id="{{product['product']._ID}}"> <i class="icon icon-heart"></i><span>Add to Wishlist</span> </a>
                   {% endif %} 
                  {% endif %}
                </div>
                <div class="product-item-details">
                  <div class="product-item-name">
                    <a title="{{product['product']._Name}}" href="{{base_url('product_detail/')~product['product']._ID}}" class="product-item-link">{{product['product']._Name}}</a>
                  </div>
                  <div class="price-box">
                    <span class="price-container">
                      <span class="price-wrapper">
                       
                        {% if product._Sellprice > 0 %}
                        <span class="price-wrapper">
                          <span class="old-price">${{ product._Price }}</span>
                          <span class="special-price">${{ product._Sellprice }}</span>
                        </span>
                        {% else %}
                        <span class="price">
                          ${{ product._Price }}
                        </span>
                        {% endif %}

                       
                      
                      </span>
                    </span>
                  </div>
                  <!-- <div class="product-item-rating">
                    <div class="rateitshow" data-rateit-value="<?php // echo $this->Mrating->get_average_total_rating($product['id'])->average + 1;?>"></div>
                  </div> -->
                  <a href="{{base_url('addtocart/')~product['product']._ID}}">
                              <button class="btn add-to-cart" data-product="{{product['product']._ID}}"> <i class="icon icon-cart" ></i><span>Add to Cart</span> </button></a>
                </div>
              </div>
            </div>
          </div>
        {% endfor %}
      {% endif %}
      
              </div>
            </div>
          </div>
        </div>
        <div class="block banners-with-pad">
          <div class="container">
            <div class="row">
              <div class="col-md-7">
                <a href="#" class="banner-wrap">
                  <div class="banner style-17 autosize-text image-hover-scale" data-fontratio="4.6">
                    <img src="{{ constant('fronttheme') }}images/banners/banner-17.jpg" alt="Banner">
                    <div class="banner-caption vertb horl">
                      <div class="vert-wrapper">
                        <div class="vert">
                          <div class="text-1">Fashion collection</div>
                          <div class="text-2 text-hoverslide" data-hcolor="#ffffff"><span><span class="text">Underwear</span><span class="hoverbg"></span></span>
                          </div>
                          <div class="text-3">To take a trivial example, which of us ever undtakes
                            <br> laborious physical exercise
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
                <a href="#bannerLink" class="banner-wrap">
                  <div class="banner style-18 autosize-text image-hover-scale" data-fontratio="4.6">
                    <img src="{{ constant('fronttheme') }}images/banners/banner-18.jpg" alt="Banner">
                    <div class="banner-caption vertl horl">
                      <div class="vert-wrapper">
                        <div class="vert">
                          <div class="text-1">BIG STATMENT</div>
                          <div class="text-2">AWESOME BRA</div>
                          <div class="banner-btn text-hoverslide" data-hcolor="#fff"><span><span class="text">SPECIAL OFFERS</span><span class="hoverbg"></span></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
              <div class="col-md-5">
                <a href="#bannerLink" class="banner-wrap">
                  <div class="banner style-19 autosize-text image-hover-scale" data-fontratio="4.6">
                    <img src="{{ constant('fronttheme') }}images/banners/banner-19.jpg" alt="Banner">
                    <div class="banner-caption vertb horl">
                      <div class="vert-wrapper">
                        <div class="vert">
                          <div class="text-1">FASHION NIGHT</div>
                          <div class="text-2">HAS ARRIVED</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
                <a href="#bannerLink" class="banner-wrap">
                  <div class="banner style-20 autosize-text image-hover-scale" data-fontratio="4.6">
                    <img src="{{ constant('fronttheme') }}images/banners/banner-20.jpg" alt="Banner">
                    <div class="banner-caption vertb horr">
                      <div class="vert-wrapper">
                        <div class="vert">
                          <div class="text-1">Always Up To Date<span class="text-corner"></span></div>
                          <div class="text-2">URBAN CLOTHING</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="block bottom-space">
          <div class="container">
            <div class="row">
               
              <div class="col-md-12">
                <!-- Deal Carousel -->
                <div class="title">
                  <h2 class="custom-color">Deal of the day</h2>
                  <div class="toggle-arrow"></div>
                  <div class="carousel-arrows"></div>
                </div>
                <div class="collapsed-content">
                  <div class="deal-carousel-2 products-grid product-variant-5">
                    <!-- Product Item -->
                  
                    {% if saleProd|length > 0 %}
                    {% for sdata in saleProd %}
                    <div class="product-item large">
                      <div class="product-item-inside">
                        <div class="product-item-info">
                          <!-- Product Photo -->
                          
                          <div class="product-item-photo">
                  {% if sdata['product']['_Image'] |length == 1 %}
                 
                  <div class="product-item-gallery">
                    <div class="product-item-gallery-main">
                      <a href="{{base_url('product_detail/')~sdata['product']._ID}}"><img class="product-image-photo" src="assets/uploads/product/{{ sdata['product']['_Image'][0]['cat'] }}" alt="product" height="200"></a>
                    </div>
                  </div>
                  {% endif %}
                  {% if sdata['product']['_Image'] |length> 1 %}
                    <div class="carousel-inside slide" data-ride="carousel">
                      <div class="carousel-inner" role="listbox">
                        {% set i = 0%}
                        {% for image in sdata['product']['_Image'] %}

                        {% if(i == 0) %}
                          <div class="item active">
                            <a href="{{base_url('product_detail/')~sdata['product']._ID}}">
                              <img class="product-image-photo" src="assets/uploads/product/{{ image['cat'] }}" alt="" />
                            </a>
                          </div>
                        {% else %}
                          <div class="item">
                            <a href="{{base_url('product_detail/')~sdata['product']._ID}}">
                              <img class="product-image-photo" src="assets/uploads/product/{{ image['cat'] }}" alt="" />
                            </a>
                          </div>
                        {% endif %}
                        {% set i = i + 1 %}
                        {% endfor %}
                      </div>
                      <a class="carousel-control next"></a>
                      <a class="carousel-control prev"></a>
                    </div>
                  {% endif %}

                  <a href="{{base_url('quick_view/')~sdata['product']._ID}}" title="Quick View" class="quick-view-link quick-view-btn">
                    <i class="icon icon-eye"></i>
                    <span>Quick View</span>
                  </a>
                  {% if get_cookie('userid') == '' %}

                  <a data-toggle="modal" data-target="#modal3" title="Add to Wishlist" class="no_wishlist add"> <i class="icon icon-heart"></i><span>Add to Wishlist</span> </a>
                  
                  {% else %}
              

                  {% if sdata['product'].wishlists |length > 0 %}
                    <a onclick ="ecommerce.notifyWithtEle('product is already exist in wishlist' , 'success' ,'topRight', 2000);"  title="In Wishlist" class="no_wishlist" style="color:#7e57c2 !important;"> <i class="icon icon-heart"></i><span>In Wishlist</span> </a>
                  {% else %}
                    <a href="{{base_url('addwishlist/')~sdata['product']._ID}}" title="Add to Wishlist" class="no_wishlist add" data-id="{{sdata['product']._ID}}"> <i class="icon icon-heart"></i><span>Add to Wishlist</span> </a>
                   {% endif %} 
                  {% endif %}
                </div>
                          <!-- /Product Photo -->
                          <!-- Product Details -->
                          <div class="product-item-details">
                            <div class="product-item-name"> <a title="Cover up tunic" href="product.html" class="product-item-link">{{sdata['product']._Name}}</a></div>
                            <div class="product-item-description">{{sdata['product']._Des}}</div>
                            <div class="price-box"> <span class="price-container"> <span class="price-wrapper"> <span class="price">{{sdata['product']._Sellprice}}</span></span>
                              </span>
                            </div>
                            <div class="product-item-rating"> <i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i><i class="icon icon-star-fill"></i></div>
                            <a href="{{base_url('addtocart/')~sdata['product']._ID}}">
                            <button class="btn add-to-cart" data-product="789123"> <i class="icon icon-cart" ></i><span>Add to Cart</span> </button></a>
                          </div>
                          <!-- /Product Details -->
                        </div>
                      </div>
                    </div>
                    <!-- /Product Item -->
                    {% endfor %}
                    {% endif %}
                 
                  </div>
                </div>
                <!-- /Deal Carousel -->
              </div>
            </div>
          </div>
        </div>
        <div class="block fullwidth full-nopad">
          <div class="container">
            <div id="instafeed" class="instagramm-feed-full"></div>
            <div class="instagramm-title">Instagram Feed</div>
          </div>
        </div>
      </main>
      <!-- /Page Content -->
        <div class="modal modal-countdown fade zoom info success" data-interval="10000" id="modal3">
    <div class="modal-dialog">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">&#10006;</button> -->
      </div>
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-center">
            <div class="icon-info"><i class="icon icon-alert"></i></div>
            <p><a href="{{base_url('login')}}">Please must be login</a></p>
          </div>
         
        </div>
      </div>
    </div>
  </div>

{% endblock %}
{% extends 'front/app/index.php' %}

{% block content %} 
  <!-- Loader -->
  <div id="loader-wrapper" class="off">
    <div class="cube-wrapper">
      <div class="cube-folding">
        <span class="leaf1"></span>
        <span class="leaf2"></span>
        <span class="leaf3"></span>
        <span class="leaf4"></span>
      </div>
    </div>
  </div>
  <!-- /Loader -->
  <div class="fixed-btns">
    <!-- Back To Top -->
    <a href="#" class="top-fixed-btn back-to-top"><i class="icon icon-arrow-up"></i></a>
    <!-- /Back To Top -->
  </div>

    <!-- Page Content -->
        <main class="page-main">
        <div class="block">
          <div class="container">
            <ul class="breadcrumbs">
              <li><a href="index.html"><i class="icon icon-home"></i></a></li>
              <li>/<span>Shopping Cart</span></li>
            </ul>
          </div>
        </div>
        <div class="block">
          <div class="container">
            <div class="cart-table">
              <div class="table-header">
                <div class="photo">
                  Product Image
                </div>
                <div class="name">
                  Product Name
                </div>
                <div class="price">
                  Unit Price
                </div>
                <div class="qty">
                  Qty
                </div>
                <div class="subtotal">
                  Subtotal
                </div>
            
              </div>
              {% if cart_data|length > 0 %}
              {% set price = 0 %}
              {% for cdata in cart_data %}
              {% set price = price + cdata._Subtotal %}
              <div class="table-row">
                <div class="photo">
                 
                  {% if cdata['product']['_Image'] != '' %}
                    <a href="product.html"><img src="{{base_url()}}assets/uploads/product/{{cdata['product']._Image[0]['thumb'] }}" alt=""></a>
                  {% else %}
                    <a href="product.html"><img src="{{ constant('fronttheme') }}images/products/product-1.jpg" alt=""></a>
                  {% endif %}
                </div>
                <div class="name">
                  <a href="{{base_url('product_detail/')~cdata._ProductID}}"> {{cdata['product']['_Name']}}</a>
                </div>
                <div id="price{{cdata._ID}}" class="price" >
                 {{cdata._Unitprice}}
                </div>
                <div class="qty qty-changer">
                  <fieldset>
                    <input type="text" class="qty-input" value="{{cdata._Quantity}}" data-min="0" data-max="5000" name="qty_value" id="qty_value{{cdata._ID}}">
                  </fieldset>
                </div>
                <div class="subtotal cart_subtotal" id="main_price{{cdata._ID}}" subtotal="{{cdata._Subtotal}}" unit="{{cdata._Unitprice}}">
                  {{cdata._Subtotal}}
                </div>
               
              </div>
              {% endfor %}
              {% endif %}
        
            </div>

            
            <div class="row">
              <div class="col-md-3 total-wrapper">
                <table class="total-price">
                  
                  <tr>
                    <td>Subtotal</td>
                    <td>${{ new_order_data[0]['_Subtotal'] }}</td>
                  </tr>
               
                 <tr>
                  {% if new_order_data[0]['_Discount'] != 0 %}
                    <td>Discount</td>
                    <td>
                        <br>(${{new_order_data[0]['_Discount']}})
                        <br>({{new_order_data[0]['coupon']['_Code']}})(${{new_order_data[0]['_Discount']}})
                    </td>
                    {% endif %}
                  </tr>
                 
                  <tr class="total">
                    <td>Grand Total</td>
                    <td id="main_cart_total" data-price="{{ price }}">${{ new_order_data[0]['_Grandtotal'] }}</td>
                  </tr>
                </table>
                <div class="cart-action">
                  <div>
                    <a href="{{base_url('fetchaddress')}}" class="btn" style="color: #ffffff" class="btn">Checkout</a>
                  </div>
                 <!--  <a href="#">Checkout with Multiple Addresses</a> -->
                </div>
              </div>
               <div class="col-sm-6 col-md-5">
                <h2>Basic Detail</h2>
                {% if loguser_id is not defined %}
                <a href="{{base_url('login')}}" class="btn">Login</a>
                {% else %}
                 <form class="white" action="#">
                  <label>Name<span class="required">*</span></label>
              <input type="text" class="form-control input-lg" name="email" readonly="true" value="{{cart_data[0].user.fullname}}">
                  <label>E-mail<span class="required">*</span></label>
              <input type="text" class="form-control input-lg" name="email" readonly="true" value="{{cart_data[0].user._Email}}">
              <label>Mobile No<span class="required">*</span></label>
              <input type="text" class="form-control input-lg" name="email" readonly="true" value="{{cart_data[0].user._Mobile}}">
                  
                </form>
                {% endif %}
               
              </div>
              <!--<div class="col-sm-6 col-md-4">
                <h2>Discount Codes</h2>
                <form class="white" action="#">
                  <label>Enter your coupon code if you have one.</label>
                  <input type="text" class="form-control dashed">
                  <div>
                    <button class="btn btn-alt">Apply Coupon</button>
                  </div>
                </form>
              </div> -->
            </div>
          </div>
        </div>
      </main>
      <!-- /Page Content -->

{% endblock %}
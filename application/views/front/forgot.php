{% extends 'front/app/index.php' %}

{% block content %} 
<main class="page-main">
	<div class="block">
		<div class="container">
			<ul class="breadcrumbs">
				<li><a href="index.html"><i class="icon icon-home"></i></a></li>
				<li>/<span>Forgot Password</span></li>
			</ul>
		</div>
	</div>
	<div class="block">
		<div class="container">
			<div class="row row-eq-height">
				<div class="col-sm-6">
					<div class="form-card">
						<h4>New Customers</h4>
						<p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
						<div><a href="{{ base_url('register') }}" class="btn btn-lg"><i class="icon icon-user"></i><span>Create An Account</span></a></div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-card">
						<h4>Registered Customers</h4>
						<form class="account-create" method="post" id="forgot_form" >
							<label>E-mail<span class="required">*</span></label>
							<input type="text" class="form-control input-lg" name="email">
							<div>
								<input type="submit" class="btn btn-lg" value="Submit"><span class="required-text">* Required Fields</span></div>
							<div class="back"><a href="{{ base_url('login') }}">Already have an Account? Login</a></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
{% endblock %}
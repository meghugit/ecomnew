{% extends 'front/app/index.php' %}

{% block content %} 
  <!-- Loader -->
  <div id="loader-wrapper" class="off">
    <div class="cube-wrapper">
      <div class="cube-folding">
        <span class="leaf1"></span>
        <span class="leaf2"></span>
        <span class="leaf3"></span>
        <span class="leaf4"></span>
      </div>
    </div>
  </div>
  <!-- /Loader --> 
  <div class="fixed-btns">
    <!-- Back To Top -->
    <a href="#" class="top-fixed-btn back-to-top"><i class="icon icon-arrow-up"></i></a>
    <!-- /Back To Top -->
  </div>

    <!-- Page Content -->
      <main class="page-main">
        <div class="block">
          <div class="container">
            <ul class="breadcrumbs">
              <li><a href="index.html"><i class="icon icon-home"></i></a></li>
              <li>/<a href="#">Women</a></li>
              <li>/<span>Black swimsuit with bindings</span></li>
              <li class="product-nav">
                <i class="icon icon-angle-left"></i><a href="#" class="product-nav-prev">prev product
                  <span class="product-nav-preview">
                    <span class="image"><img src="images/products/product-prev-preview.jpg" alt=""><span class="price">$280</span></span>
                    <span class="name">Black swimsuit</span>
                  </span></a>/
                <a href="#" class="product-nav-next">next product
                  <span class="product-nav-preview">
                    <span class="image"><img src="images/products/product-next-preview.jpg" alt=""><span class="price">$280</span></span>
                    <span class="name">Black swimsuit</span>
                  </span></a><i class="icon icon-angle-right"></i>
              </li>
            </ul>
          </div>
        </div>
        <div class="block product-block">
          <div class="container">
            <div class="row">
            	<div class="col-sm-6 col-md-6 col-lg-4">
				  	<!-- Product Gallery -->
				  	<div class="main-image">
				      <img src="assets/uploads/product/{{ fetch_data[0]['_Image'][0]['large'] }}" class="zoom" alt="{{fetch_data[0]._Name}}" data-zoom-image="assets/uploads/product/{{ fetch_data[0]['_Image'][0]['large'] }}" />
				      <div class="dblclick-text"><span>Double click for enlarge</span></div>
				      <a href="assets/uploads/product/{{ fetch_data[0]['_Image'][0]['large'] }}" class="zoom-link"><i class="icon icon-zoomin"></i></a>
				    </div>
				    <div class="product-previews-wrapper">
						<div class="product-previews-carousel" id="previewsGallery">
				    	{% if count(fetch_data[0]['_Image']) > 0 %}
				    	{% for image in fetch_data[0]['_Image'] %}
				                <a href="#" data-image="assets/uploads/product/{{image.large}}" data-zoom-image="assets/uploads/product/{{image.name}}"><img src="assets/uploads/product/{{image.thumb}}" alt="" /></a>
				    	{% endfor %}
				    	{% endif %}
				      	</div>
				    </div>
				    <!-- /Product Gallery -->
			  	</div>
              <div class="col-sm-6 col-md-6 col-lg-8">
                <div class="product-info-block classic">
                  <div class="product-info-top">
                    <div class="product-sku">SKU: <span>Stock Keeping Unit</span></div>
                    <div class="rating">
                      <i class="icon icon-star fill"></i><i class="icon icon-star fill"></i><i class="icon icon-star fill"></i><i class="icon icon-star fill"></i><i class="icon icon-star"></i><span class="count">248 reviews</span>
                    </div>
                  </div>
                  <div class="product-name-wrapper">
                    <h1 class="product-name">{{fetch_data[0]._Name}}</h1>
                    <div class="product-labels">
                      <span class="product-label sale">SALE</span>
                      <span class="product-label new">NEW</span>
                    </div>
                  </div>
                  <div  id="quan_avble" ></div>
                  <div class="product-description">
                    <p>{{fetch_data[0]._Des}}</p>
                  </div>
                  

                    
                    {% if count(attr_data) > 0 %}
                    {% for atdata in attr_data %}
                    {% for attdetails in adet_data %}
                    {% if atdata._ID == attdetails._AttID %}
                    {% set val_arr = explode(',',atdata._Values) %}
                    <div class="product-options">
                      <div class="product-size swatches">
                      <span class="option-label">{{atdata._Name}}:</span>
                      <input type="hidden" name="attid" id="attid" value="{{atdata._ID}}">

                      <ul class="size-list">
                        {% for val_data in attdetails._Attinfo %}
                        <!-- {{print_r(attd_data.id)}} -->
                            <li p_id="{{fetch_data[0]._ID}}" att_detail="{{val_data.id}}" class="attr_data_get"><a href="#" ><span class="value">{{val_data.id}}</span></a></li>
                            <input type="hidden" id="attdetail" name="attdetail" value="{{val_data.id}}">
                        {% endfor %}
                      </ul>
                    </div>
                    </div>
                    {% endif %}
                    {% endfor %}
                    {% endfor %}
                    {% endif %}
                
                    <div class="product-qty">
                      <span class="option-label">Qty:</span>
                      <div class="qty qty-changer">
                        <fieldset>
                          <input type="button" value="&#8210;" class="decrease qty_change_dec_pro">
                          <input type="text" class="qty-input" value="{% if cart_data[0]._Quantity != '' %}{{cart_data[0]._Quantity}}{% else %}1{% endif %}" data-min="1" name="qty_value" id="qty_value"> 
                          <input type="button" value="+" class="increase qty_change_inc_pro">
                        </fieldset>
                      </div>
                    </div>
                  
                  
                  <div class="product-actions">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="product-meta">
                           {% if get_cookie('userid') == '' %}
                          <span><a data-toggle="modal" data-target="#modal2" ><i class="icon icon-heart"></i> Add to wishlist</a></span>
                           {% else %}
                          <span><a href="{{base_url('add_user_wishlist/'~fetch_data[0]._ID)}}" ><i class="icon icon-heart"></i> Add to wishlist</a></span>
                           {% endif %}
                          
                        </div>
                        <div class="social">
                          <div class="share-button toLeft">
                            <span class="toggle">Share</span>
                            <ul class="social-list">
                              <li>
                                <a href="#" class="icon icon-google google"></a>
                              </li>
                              <li>
                                <a href="#" class="icon icon-fancy fancy"></a>
                              </li>
                              <li>
                                <a href="#" class="icon icon-pinterest pinterest"></a>
                              </li>
                              <li>
                                <a href="#" class="icon icon-twitter-logo twitter"></a>
                              </li>
                              <li>
                                <a href="#" class="icon icon-facebook-logo facebook"></a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>

                      <div id="get_arr_data" test_data="{{ adet_data|json_encode }}"> </div>
                    <div class="col-md-6">
                        <div class="price">
                              {% if fetch_data[0]._Sellprice != '' %}
                                {% set m_price = fetch_data[0]._Sellprice %}
                              {% else %}
                                {% set m_price = fetch_data[0]._Price %}
                              {% endif %}
                            <span class="special-price" ><span id="main_price" price_val="{% if cart_data[0]._Subtotal != '' %}{{cart_data[0]._Subtotal}} {% else %}{{m_price}}
                            {% endif %}" orig_val="{{m_price}}">
                            {% if cart_data[0]._Subtotal != '' %}
                              {{cart_data[0]._Subtotal}}
                            {% else %}
                              {% if fetch_data[0]._Sellprice != '' %}
                                <strike style="color:black;">{{fetch_data[0]._Price}}</strike>
                                {{fetch_data[0]._Sellprice}}
                              {% else %}
                                {{fetch_data[0]._Price}}
                              {% endif %}
                            {% endif %}
                          </span></span>
                        </div>
                        <div class="actions" id="addto_cart">
                          <button data-loading-text='<i class=""></i><span>Add to cart</span>' class="btn btn-lg "><i class="icon icon-cart"></i><span >Add to cart</span></button>
                        <!--   <a href="#" class="btn btn-lg product-details"><i class="icon icon-external-link"></i></a>
                         --></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
         
            </div>
          </div>
        </div>
        <div class="block">
          <div class="tabaccordion">
            <div class="container">
              <!-- Nav tabs -->
              <ul class="nav-tabs product-tab" role="tablist">
                <li><a href="#Tab1" role="tab" data-toggle="tab">Description</a></li>
                <li><a href="#Tab3" role="tab" data-toggle="tab">Sizing Guide</a></li>
                <li><a href="#Tab5" role="tab" data-toggle="tab">Reviews</a></li>
              </ul>
              <!-- Tab panes -->
              {% if count(fetch_data) > 0 %}
              {% for pdata in fetch_data %}
              <div class="tab-content">
				<div role="tabpanel" class="tab-pane" id="Tab1">
                  <p>{{pdata._Des}}</p>
                  {% if pdata._Adddetails != '' %}
                  <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                      <tbody>
                        {% set i = 0 %}
                        {% for paydata in pdata._Adddetails['key'] %}
                        <tr>
                          <td><strong>{{paydata}}</strong></td>
                          <td>{{pdata._Adddetails['value'][i]}}</td>
                        </tr>
                        {% set i = i + 1 %}
                        {% endfor %}
                      </tbody>
                    </table>
                  </div>
                  {% endif %}
                </div>
                <div role="tabpanel" class="tab-pane" id="Tab2">
                  <h3 class="custom-color">Take a trivial example which of us ever undertakes</h3>
                  <p>Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure </p>
                  <ul class="marker-simple-list two-columns">
                    <li>Nam liberempore</li>
                    <li>Cumsoluta nobisest</li>
                    <li>Eligendptio cumque</li>
                    <li>Nam liberempore</li>
                    <li>Cumsoluta nobisest</li>
                    <li>Eligendptio cumque</li>
                  </ul>
                </div>
                <div role="tabpanel" class="tab-pane" id="Tab3">
                  <h3>Single Size Conversion</h3>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <tbody>
                        <tr>
                          <td><strong>UK</strong></td>
                          <td>
                            <ul class="params-row">
                              <li>18</li>
                              <li>20</li>
                              <li>22</li>
                              <li>24</li>
                              <li>26</li>
                            </ul>
                          </td>
                        </tr>
                        <tr>
                          <td><strong>European</strong></td>
                          <td>
                            <ul class="params-row">
                              <li>46</li>
                              <li>48</li>
                              <li>50</li>
                              <li>52</li>
                              <li>54</li>
                            </ul>
                          </td>
                        </tr>
                        <tr>
                          <td><strong>US</strong></td>
                          <td>
                            <ul class="params-row">
                              <li>14</li>
                              <li>16</li>
                              <li>18</li>
                              <li>20</li>
                              <li>22</li>
                            </ul>
                          </td>
                        </tr>
                        <tr>
                          <td><strong>Australia</strong></td>
                          <td>
                            <ul class="params-row">
                              <li>8</li>
                              <li>10</li>
                              <li>12</li>
                              <li>14</li>
                              <li>16</li>
                            </ul>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="Tab4">
                  <ul class="tags">
                    <li><a href="#"><span class="value"><span>Dresses</span></span></a></li>
                    <li><a href="#"><span class="value"><span>Outerwear</span></span></a></li>
                    <li><a href="#"><span class="value"><span>Tops</span></span></a></li>
                    <li><a href="#"><span class="value"><span>Sleeveless tops</span></span></a></li>
                    <li><a href="#"><span class="value"><span>Sweaters</span></span></a></li>
                  </ul>
                  <div class="divider"></div>
                  <h3>Add your tag</h3>
                  <form class="contact-form white" action="#">
                    <label>Tag<span class="required">*</span></label>
                    <input class="form-control input-lg">
                    <div>
                      <button class="btn btn-lg">Submit Tag</button>
                    </div>
                    <div class="required-text">* Required Fields</div>
                  </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="Tab5">
                  <form class="contact-form white" name="review_from" id="review_from">
                    <input type="hidden" name="pr_id" id="pr_id" value="{{fetch_data[0]._ID}}">
                  <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <td></td>
                          <td class="text-center">1 star</td>
                          <td class="text-center">2 star</td>
                          <td class="text-center">3 star</td>
                          <td class="text-center">4 star</td>
                          <td class="text-center">5 star</td>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><strong>REview</strong></td>
                          <td class="text-center">
                            <label class="radio">
                              <input id="vote-price1" type="radio" name="vote-price" value="1"><span class="outer"><span class="inner"></span></span>
                            </label>
                          </td>
                          <td class="text-center">
                            <label class="radio">
                              <input id="vote-price2" type="radio" name="vote-price" value="2"><span class="outer"><span class="inner"></span></span>
                            </label>
                          </td>
                          <td class="text-center">
                            <label class="radio">
                              <input id="vote-price3" type="radio" name="vote-price" value="3"><span class="outer"><span class="inner"></span></span>
                            </label>
                          </td>
                          <td class="text-center">
                            <label class="radio">
                              <input id="vote-price4" type="radio" name="vote-price" value="4"><span class="outer"><span class="inner"></span></span>
                            </label>
                          </td>
                          <td class="text-center">
                            <label class="radio">
                              <input id="vote-price5" type="radio" name="vote-price" value="5"><span class="outer"><span class="inner"></span></span>
                            </label>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <h3>Add new review</h3>
                  
                    <label>Title<span class="required">*</span></label>
                    <input type="text" name="title" id="title" class="form-control">
                    <label>Review<span class="required">*</span></label>
                    <textarea class="form-control input-lg" name="review" id="review"></textarea>
                   
                    {% if get_cookie('userid') == '' %}
                     <div>
                      <button class="btn btn-lg" data-toggle="modal" data-target="#modal2">Submit Review</button>
                    </div>
                    {% else %}
                     <div>
                      <button class="btn btn-lg">Submit Review</button>
                    </div>
                    {% endif %}
                   
                    <div class="required-text">* Required Fields</div>
                  </form>
                </div>
              </div>
              {% endfor %}
              {% endif %}


              <div class="block">
          <div class="review-top">
            {% if count(allreview) > 0 %}

              {% for rvdata in allreview %}
            

            <div class="container" style="border-bottom: 1px solid #e8e8e8;margin-bottom: 15px;padding-bottom: 15px;">
             
                     <div ><img class="product-image-photo" src="assets/uploads/user/{{ rvdata['users']['_Profilepic'] }}" alt="" style="height: 60px;width: 60px;border-radius: 50px"></div>
                    <div style="font-size: 15px;font-weight: normal;margin-top: -63px;margin-left: 87px;">
                       <div style="font-weight:bold">{{rvdata.users.fullname}}
                        <span style="font-weight:normal;font-size: 
                        13px;color:#999!important">{{rvdata.users.cities.name}}</span>
                       </div>
                       <div style="margin-top: 5px;text-transform: lowercase;color: black;">{{rvdata._Title}}</div>
                       <div style="font-size: 15px; color:#333745;margin-top: 10px;text-transform: lowercase; "><span class="badge badge-success">{{rvdata._Rate}} <i class="icon icon-star fill"></i></span><span class="count" style="margin-left: 10px;color:#333745;font-size:15px">{{rvdata._Review}}</span></div>
                       <div style="font-size: 15px; color:#333745;margin-top: 10px;text-transform: lowercase; ">{{rvdata._Description}}</div>
                        <br>
                    </div>
               </div>
              {% endfor %}
              {% endif %}
        </div>
  

            </div>
          </div>
        </div>
        
      </main>
      <!-- /Page Content -->

      <div class="modal modal-countdown fade zoom info success" data-interval="10000" id="modal2">
    <div class="modal-dialog">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">&#10006;</button> -->
      </div>
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-center">
            <div class="icon-info"><i class="icon icon-alert"></i></div>
            <!-- after you can give review -->
            <p><a href="{{base_url('login')}}">Please must be login </a></p>
          </div>
         
        </div>
      </div>
    </div>
  </div>

   <div class="modal modal-countdown fade zoom info success" data-interval="10000" id="modal_addtocart">
    <div class="modal-dialog">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">&#10006;</button> -->
      </div>
      <div class="modal-content">
        <div class="modal-body">
          <div class="text-center">
            <div class="icon-info"><i class="icon icon-alert"></i></div>
            <p>you can't add to cart..product is not avialble.</p>
          </div>
         
        </div>
      </div>
    </div>

{% endblock %}
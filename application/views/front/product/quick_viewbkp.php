{% include 'front/app/header.php' %}

{% block content %} 



<div class="quickview">  
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-5">
			<!-- Product Gallery -->
			<div class="main-image hover">
		      	<img src="{{base_url()}}assets/uploads/product/{{ fetch_data[0]['_Image'][0]['large'] }}" class="zoom" alt="{{fetch_data[0]._Name}}" data-zoom-image="{{base_url()}}assets/uploads/product/{{ fetch_data[0]['_Image'][0]['large'] }}" />
		      	<div class="dblclick-text"><span>Double click for enlarge</span></div>
		    </div>
		    <div class="product-previews-wrapper">
				<div class="product-previews-carousel" id="previewsGallery">
		    	{% if count(fetch_data[0]['_Image']) > 0 %}
		    	{% for image in fetch_data[0]['_Image'] %}
		                <a href="#" data-image="{{base_url()}}assets/uploads/product/{{image.large}}" data-zoom-image="{{base_url()}}assets/uploads/product/{{image.name}}"><img src="{{base_url()}}assets/uploads/product/{{image.thumb}}" alt="" /></a>
		    	{% endfor %}
		    	{% endif %}
		      	</div>
		    </div>
			<!-- /Product Gallery -->
		</div>
		<div class="col-sm-7">
			<div class="product-info-block classic">
				<div class="product-info-top">
                    <div class="rating">
                      <div class="rateitshow" data-rateit-value="3">
                      <span class="count">248 reviews</span>
                    </div>
                  </div>
				<div class="product-name-wrapper">
                    <h1 class="product-name">{{fetch_data[0]._Name}}</h1>
                    <input type="hidden" name="pr_id" id="pr_id" value="{{fetch_data[0]._ID}}">
              	</div>
			 	<div id="quan_avble" ></div>
              	<div class="product-description">
                    <p>{{fetch_data[0]._Des}}</p>
              	</div>
				{% if count(attr_data) > 0 %}
                {% for atdata in attr_data %}
                {% for attdetails in adet_data %}
                {% if atdata._ID == attdetails._AttID %}
                {% set val_arr = explode(',',atdata._Values) %}
                <div class="product-options">
                  	<div class="product-size swatches">
                  		<span class="option-label">{{atdata._Name}}:</span>
                  		<ul class="size-list">
                			{% for val_data in attdetails._Attinfo %}
                        		<li p_id="{{fetch_data[0]._ID}}" att_detail="{{val_data.id}}" class="attr_data_get"><a href="#" ><span class="value">{{val_data.id}}</span></a></li>
                    		{% endfor %}
                  		</ul>
                	</div>
                </div>
                {% endif %}
                {% endfor %}
                {% endfor %}
                {% endif %}	

                <div class="product-qty">
                  	<span class="option-label">Qty:</span>
                  	<div class="qty qty-changer">
                    	<fieldset>
                      		<input type="button" value="&#8210;" class="decrease qty_change_dec_pro">
                  			<input type="text" class="qty-input"  data-min="1" name="qty_value" id="qty_value" value="{% if cart_data[0]._Quantity != '' %}{{cart_data[0]._Quantity}}{% else %}1{% endif %}"> 
                      		<input type="button" value="+" class="increase qty_change_inc_pro">
                    	</fieldset>
                  	</div>
                </div>

				<div class="product-actions">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="product-meta">
                          <span><a href="{{base_url('add_user_wishlist/'~fetch_data[0]._ID)}}" ><i class="icon icon-heart"></i> Add to wishlist</a></span>
                        </div>
                      </div>

                      <div id="get_arr_data" test_data="{{ adet_data|json_encode }}"> </div>
                		<div class="col-md-6">
                        <div class="price">
                              {% if fetch_data[0]._Sellprice != '' %}
                                {% set m_price = fetch_data[0]._Sellprice %}
                              {% else %}
                                {% set m_price = fetch_data[0]._Price %}
                              {% endif %}

                            <span class="special-price" ><span id="main_price" price_val="{% if cart_data[0]._Subtotal != '' %}{{cart_data[0]._Subtotal}} {% else %}{{m_price}}
                            {% endif %}" orig_val="{{m_price}}">
                            {% if cart_data[0]._Subtotal != '' %}
                              {{cart_data[0]._Subtotal}}
                            {% else %}
                              {% if fetch_data[0]._Sellprice != '' %}
                                <strike style="color:black;">{{fetch_data[0]._Price}}</strike>
                                {{fetch_data[0]._Sellprice}}
                              {% else %}
                                {{fetch_data[0]._Price}}
                              {% endif %}
                            {% endif %}
                            </span></span>
                        </div>


                        <div class="actions" id="quickview_id">
                          <a >
                              <button data-loading-text='<i class=""></i><span>Add to cart</span>' class="btn btn-lg " id=""><i class="icon icon-cart"></i><span >View Product</span></button>
                          </a>
                        <!--   <a href="#" class="btn btn-lg product-details"><i class="icon icon-external-link"></i></a>
                         --></div>
                      	</div>
                    </div>
              	</div>
			</div>
		</div>
	</div> 
</div>
</div>
</div>
{% endblock %}

{% block scripts %}
<script type="text/javascript">

  $('document').on('click','#quickview_id',function(){
    alert("ggg");
  })

	$(".rateit").bind('rated', function (event, value) { $('#rating').val(value - 1); });
    $(".rateit").bind('reset', function () { $('#rating').val(0); });

    $('.rateitshow').each(function(){  
        $(this).rateit({ 
            readonly: true,
            resetable: false,
            value: $(this).data('value') ? $(this).data('value') : 0,
            min: 1,
            max: 6,
            step: 0.5
        });
    });
	var $body = $('body');
	$.fn.initProductZoom = function () {
		var $this = this,
			zoompos = $body.is('.rtl') ? 11 : 1;
		if (!$body.is('.touch')) {
			$this.ezPlus({
				gallery: 'previewsGallery',
				galleryActiveClass: 'active'
			});
		} else {
			$this.ezPlus({
				gallery: 'previewsGallery',
				galleryActiveClass: 'active'
			});
		}
	}

	if ($(".main-image").length) {
		$('.main-image > .zoom').initProductZoom();
	}

	// product previews carousel
	if ($(".product-previews-carousel").length) {

		var $this = $(".product-previews-carousel");

		$this.slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			dots: false,
			focusOnSelect: true,
			infinite: false
		});

		$this.on('click', '.slick-slide', function () {
			$('.zoom-link').removeClass('disable-gallery');
		})
  }

     $('.qty_change_dec_pro').click(function(){
          old_q = $('#qty_value').val();
          new_q = parseInt(old_q) - 1;
          old_price = $('#main_price').attr('price_val');
          orig_val = $('#main_price').attr('orig_val');
          new_price = parseInt(old_price) - parseInt(orig_val);
          $('#main_price').attr('price_val',new_price);
          $('#main_price').html(new_price);
          $('#qty_value').val(new_q);
      });

      $('.qty_change_inc_pro').click(function(){
          old_q = $('#qty_value').val();
          new_q = parseInt(old_q) + 1;
          old_price = $('#main_price').attr('price_val');
          orig_val = $('#main_price').attr('orig_val');
          new_price = parseInt(old_price) + parseInt(orig_val);
          $('#main_price').attr('price_val',new_price);
          $('#main_price').html(new_price);
          $('#qty_value').val(new_q);
      });

        $('#addto_cart').click(function(){
            price = $('main_price').attr('orig_val');
            stotal = $('main_price').attr('price_val');
            qty = $('#qty_value').val();
            pid = $('#pr_id').val();

             $.ajax({
                url:'../addtocart_cart/'+pid,
                type:'POST',
                data: {price:price,qty:qty,pid:pid,stotal:stotal},
                datatype : "application/json",
                success:function(response){
                     var res = $.parseJSON(response);
                }
            });

         

            });
          
          $('#quickview_id').click(function(){
            alert("gg");
             // window.loaction = base_url+'product_detail/1';
          });
	
</script>

<script type="text/javascript">

     

</script>
{% endblock %}
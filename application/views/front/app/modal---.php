<!-- SIMPLE AJAX MODAL -->
<script type="text/javascript">
    function showLargeAjaxModal(url,title)
    {
        // SHOWING AJAX PRELOADER IMAGE
        jQuery('#large_modal_ajax .modal-body').html('<div style="text-align:center;"></div>');
//<img src="<?php echo IMG_PATH;?>loading.gif" style="max-width: 150px;"/>
        // LOADING THE AJAX MODAL
        jQuery('#large_modal_ajax').modal('show', {backdrop: 'true'});
        
        // SHOW AJAX RESPONSE ON REQUEST SUCCESS
        $.ajax({
            url: url,
            success: function(response)
            {
                jQuery('#large_modal_ajax .modal-title').html("");
                jQuery('#large_modal_ajax .modal-body').html("");
                jQuery('#large_modal_ajax .modal-title').html('<strong>' + title + '</strong>');
                jQuery('#large_modal_ajax .modal-body').html(response);
            }
        });
    }
</script>

<div class="modal fade zoom" id="large_modal_ajax" tabindex="-1" role="dialog" aria-hidden="true" data-pause='1500'>
    <div class="modal-dialog" style="width:80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
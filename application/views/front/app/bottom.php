<!--  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script> -->


  <script src="{{ constant('fronttheme') }}js/vendor/jquery/jquery.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/bootstrap/bootstrap.min.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/swiper/swiper.min.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/slick/slick.min.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/parallax/parallax.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/isotope/isotope.pkgd.min.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/magnificpopup/dist/jquery.magnific-popup.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/countdown/jquery.countdown.min.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/nouislider/nouislider.min.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/ez-plus/jquery.ez-plus.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/tocca/tocca.min.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/bootstrap-tabcollapse/bootstrap-tabcollapse.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/scrollLock/jquery-scrollLock.min.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/darktooltip/dist/jquery.darktooltip.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
  <script src="{{ constant('fronttheme') }}js/vendor/instafeed/instafeed.min.js"></script>
  <script src="{{ constant('fronttheme') }}js/megamenu.min.js"></script>
 
  <script src="{{ constant('fronttheme') }}js/app.js"></script>

  <script type="text/javascript" src="{{ constant('cmstheme') }}js/noty.min.js"></script>

  <script src="{{ constant('cmstheme') }}js/jquery.validate.min.js"></script> 
  <script src="{{ constant('cmstheme') }}plugins/sweetalert2/sweetalert2.all.min.js"></script>
  <script src="{{ constant('fronttheme') }}js/front.js"></script>
  <script src="{{ constant('fronttheme') }}js/tools.min.js"></script>

{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1>Add Payment</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
              <li class="breadcrumb-item active">Add Payment</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
               <div class="row">
              <div class="col-md-12">
              
              <form  id="payment_form" name="payment_form" method="post" enctype="multipart/form-data">
                {% if editid is not defined %}
                   <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text"  class="form-control" id="payname" name="payname" placeholder="Enter Name" >
                  </div>
                   <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="paydes" name="paydes" placeholder="Enter Description" cols="3"></textarea>
                  </div>
                
                  <input type="submit" class="btn btn-success" value="create" style="margin-top: 10px;">
                {% else %}  
                {% if editpay|length > 0 %}

                  <input type="hidden" id="hpayid" name="hpayid" value="{{editpay._ID}}">
                  <input type="hidden" id="_payname" name="_payname" value="{{editpay._Name}}">
                 <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text"  class="form-control" id="payname" name="payname" placeholder="Enter Name" value="{{editpay._Name}}" >
                  </div>
                   <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="form-control" id="paydes" name="paydes" placeholder="Enter Description" cols="3">{{editpay._Description}}</textarea>
                  </div>
                
                  <input type="submit" class="btn btn-success" value="save" style="margin-top: 10px;">

                  {% endif %}
                  
                  {% endif %}
              </form>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}

    {% block scripts  %}
    <script type="text/javascript">
        ecommerce._payment();
    </script>
    {% endblock %}

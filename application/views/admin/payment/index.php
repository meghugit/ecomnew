{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

      <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2"> 
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Payment Settings</h1>
          </div>
           <div class="col-sm-6">
           <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
           <li class="breadcrumb-item active">Payment Settings</li>
         </ol>
       </div> 
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card  card-default">
            <div class="card-header">
        <div class="card-tools">
            <a href="{{base_url('createpayment')}}" class="btn  btn-block bg-gradient-primary pull-right"><i class="fas fa-plus"></i> Add Payment</a>
        </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="user_tab" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>{% if allpayment|length > 0 %}
              
            <button class="btn btn-danger" name="delete_all" id="delete_all" class="glyphicon glyphicon-pencil"><i class="fas fa-user-slash"></i></button>
            {% endif %}</th> 
                  <th>No.</th>
                  <th>Name</th>
                  <th>Description</th> 
                  <th>Details</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  {% if allpayment|length > 0 %}

                
                  {% set i = 1 %}
                    {% for pdata in allpayment %}
                    
                <tr>
                  <td><input type="checkbox" class="delete_checkbox" name="checkuser[]" id="checkuser" value="{{pdata._ID}}"></td>

                  <td>{{i}}</td>
                  <td>{{pdata._Name}}</td>
                  <td>{{pdata._Description}}</td>
                  <td>
                    <a href="{{ base_url('paymentdetails/'~pdata._ID) }}" class="btn btn-primary"><i class="fas fa-info-circle"></i> details</a>
                  </td>
                  <td>

                     <a href="{{ base_url('editpayment/'~pdata._ID) }}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
            
                  </td>
                </tr>
                
                  {% set i = i + 1 %}
                 {% endfor %}
                
               {% endif %}
                </tbody>
              
            </table>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

  
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</section>

  </div>
  <!-- /.content-wrapper -->

  {% endblock %}

  {% block scripts %}
    <script type="text/javascript">
        ecommerce._payment();
<<<<<<< HEAD
         $(document).ready(function () {
         $('#delete_all').click(function(){
      var checkbox = $('.delete_checkbox:checked');
  
      if(checkbox.length > 0)
      {
       var checkbox_value = [];
       $(checkbox).each(function(){
        checkbox_value.push($(this).val());
       });
     
       Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!',
              showLoaderOnConfirm: true,
              preConfirm: () => {
                    $.ajax({
                            type: 'POST',
                            url: base_url+'delpayment',
                            data:{id:checkbox_value},
                            success: function(response){
                                var temp = JSON.parse(response);
                                    if(temp.type == 'success'){
                                        Swal.fire({
                                                title: temp.msg,
                                                type: "success",
                                                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                                                confirmButtonText: 'Ok',
                                                closeOnConfirm: false,
                                                onAfterClose:()=>{
                                                  redirecturl(temp.url)
                                                }
                                        })
                                    }else {
                                        Swal.fire("No records found", '', "error");
                                    }

                            }

                        });
              }
        }) ;
   function  redirecturl(url =""){
                 window.location.href = url;
              }

  }
  else
  {
   alert('Select atleast one records');
  }

 });
    })
=======
        ecommerce._delpayment();
>>>>>>> 1387377960807901e00344fd01a10caa7cc72e84
    </script>
    {% endblock %}

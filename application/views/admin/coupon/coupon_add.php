{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1>Coupons</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
              <li class="breadcrumb-item active">Coupons</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              
              <form  id="coupon_form" name="coupon_form" method="post" enctype="multipart/form-data">
                {% if editcouponid is not defined %}
                <div class="row">
                <div class="col-md-6">
                   <div class="form-group">
                    <label for="code">Coupon code</label>
                    <input type="text"  class="form-control" id="code" name="code" placeholder="Enter Code"  >
                  </div>
                   <div class="form-group">
                    <label for="description">Description</label>
                     <textarea  name="coupondes" class="form-control" id="coupondes"  placeholder="Enter Description"></textarea>
                  </div>
                   <div class="form-group">
                    <label for="type">Coupon type</label>
                    <select name="coupontype" class="form-control" id="coupontype">
                      <option value="">--Please select--</option>
                      <option value="1">percentage</option>
                      <option value="2">price</option>
                    </select>
                   
                  </div>
                   <div class="form-group">
                    <label for="amount">Coupon amount</label>
                    <input type="text" name="amount" class="form-control" id="amount"  placeholder="Enter Amount" >
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="fromdate">From date</label>
                     <div class="input-group date" id="picker1" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" data-target="#picker1" name="fromdate" id="fromdate" />
                      <div class="input-group-append" data-target="#picker1" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                      </div>
                      </div>
                  </div>

                    <div class="form-group">
                    <label for="todate">To date</label>
                    <div class="input-group date" id="picker2" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" data-target="#picker2" name="todate" id="todate" />
                      <div class="input-group-append" data-target="#picker2" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                      </div>
                      </div>
                  </div>

                  <div class="form-group">
                    <label for="reg_input_logo" >Image</label>
                      <div class="custom-file">
                      <input type="file" class="custom-file-input" id="couponimage" name="couponimage">
                      <label class="custom-file-label" for="couponimage">Choose file</label>
                    </div>             
                  </div>

                  <div class="form-group">
                  <label>Category</label>

                   <select class="form-control " name="category" id="category" data-placeholder="Select a category" style="width: 100%;">
                    <option value="">Select a category</option>
                    {% if category|length > 0 %}
                    {% for catdata in category %}
                    <option value="{{catdata._ID}}">{{catdata._Name}}</option>
                    {% endfor %}
                    {% endif %}
                    </select>
                  </div>

                  <div class="form-group subcat_show" id="sub_category" style="display: none;">
                 <label>Sub Category</label>
                      <select class="form-control select2" multiple="multiple" name="subcategory[]" id="subcat_options" data-placeholder="Select a Sub category" style="width: 100%;">
                      </select>
                </div>

                  <div class="form-group">
                     <label for="exampleInputPassword1">Status</label>
                    <div class="custom-control custom-switch">
                      <input type="checkbox" class="custom-control-input" id="status" name="status">
                      <label class="custom-control-label" for="status"></label>
                    </div>
                  </div>
                </div>
                <input type="submit" class="btn btn-success" value="submit" style="margin-top: 10px;">
              </div>
                {% else %}
                  {% if editcoupondata|length > 0 %}

                  <div class="row">
                    <div class="col-md-6">
                   <div class="form-group">
                    <label for="code">Coupon code</label>
                    <input type="text"  class="form-control" id="code" name="code" placeholder="Enter Code" value="{{editcoupondata._Code}}" >
                  </div>
                    <input type="hidden" id="hcouponid" name="hcouponid" value= "{{editcoupondata._ID}}"/>
                   <div class="form-group">
                    <label for="description">Description</label>
                     <textarea  name="coupondes" class="form-control" id="coupondes"  placeholder="Enter Description">{{editcoupondata._Description}}</textarea>
                  </div>
                   <div class="form-group">
                    <label for="type">Coupon type</label>
                    <select name="coupontype" class="form-control" id="coupontype">
                      <option value="">--Please select--</option>
                      <option value="1" {% if editcoupondata._Type == 1 %} selected {% endif %}>percentage</option>
                      <option value="2" {% if editcoupondata._Type == 2 %} selected {% endif %}>price</option>
                    </select>
                  </div>
                   <div class="form-group">
                    <label for="amount">Coupon amount</label>
                    <input type="text" name="amount" class="form-control" id="amount"  placeholder="Enter Amount" value="{{editcoupondata._Amount}}" >
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="fromdate">From date</label>
                     <div class="input-group date" id="picker1" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" data-target="#picker1" name="fromdate" id="fromdate" value="{{editcoupondata._Fromdate}}" />
                      <div class="input-group-append" data-target="#picker1" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                      </div>
                      </div>
                  </div>

                    <div class="form-group">
                    <label for="todate">To date</label>
                    <div class="input-group date" id="picker2" data-target-input="nearest">
                      <input type="text" class="form-control datetimepicker-input" data-target="#picker2" name="todate" id="todate" value="{{editcoupondata._Todate}}" />
                      <div class="input-group-append" data-target="#picker2" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                      </div>
                      </div>
                  </div>

                  <div class="form-group">
                    <label for="reg_input_logo" >Image</label>
                      <div class="custom-file">
                      <input type="file" class="custom-file-input" id="couponimage" name="couponimage">
                      <label class="custom-file-label" for="couponimage">Choose file</label>
                      <img src="{{base_url()}}assets/uploads/coupon/{{editcoupondata._Image}}" style="width: 70px;height:70px;border-radius: 50px;margin-left: 50px;margin-top: 15px;">
                    </div>             
                  </div>

                    <div class="form-group">
                 <select class="form-control " name="category" id="category" data-placeholder="Select a category" style="width: 100%;">
                    <option value="">Select a category</option>
                    {% if category|length > 0 %}
                    {% for catdata in category %}
                    <option value="{{catdata._ID}}" selected="{% if catdata._ID in editcoupondata._CatID %} selected {% endif %}">{{catdata._Name}}</option>
                    {% endfor %}
                    {% endif %}
                    </select>
                  </div>

                  <div class="form-group subcat_show" id="sub_category" >

                    <label>Sub Category</label>
                    <select class="form-control select2" multiple="multiple" name="subcategory[]" id="subcat_options" data-placeholder="Select a Sub category" style="width: 100%;">
                      {% if subcategory|length > 0 %}
                      {% for scdata in subcategory %}
                      {{scdata._ID}}
                      <option value="{{scdata._ID}}" {% if scdata._ID in editcoupondata._Subcatid %} selected="selected" {% endif %}>{{scdata._Name}}</option>

                      {% endfor %}
                      {% endif %}
                      </select>
                  </div>

                  <div class="form-group">
                     <label for="exampleInputPassword1">Status</label>
                    <div class="custom-control custom-switch">
                      <input type="checkbox" class="custom-control-input" id="status" name="status" {% if editcoupondata._Status == 1 %} checked {% endif %}>
                      <label class="custom-control-label" for="status"></label>
                    </div>
                  </div>
                </div>
                <input type="submit" class="btn btn-success" value="submit" style="margin-top: 10px;">
              </div>
                {% endif %}
              {% endif %}
              </form>
          
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}
  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/coupon.js"></script>
  
  {% endblock %}
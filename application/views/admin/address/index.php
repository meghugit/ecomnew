{% extends 'admin/app/index.php' %}

{% block content %}

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1>Address</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
              <li class="breadcrumb-item active">Address</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
    <section class="content">
    	<div class="card card-solid">
        	<div class="card-body pb-0">
        		<div class="row">
				    <div class="col-sm-12" style="margin-bottom: 15px;">
			            <button type="button" class="btn btn-primary" onclick="ecommerce.showAjaxModal('admin/address/edit/','Address Create', 'modal-lg');"><span class="m-r-5"><i class="fa fa-plus"></i></span>Add New address</button>
				    </div>
				</div>
          <div class="row d-flex align-items-stretch">

          	{% if count(addresses) > 0 %}
				{%for address in addresses %}

            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-7">
                      <h2 class="lead"><b>{{ address._Name }}</b></h2>
                      <p class="text-muted text-sm"></p>
                      <ul class="ml-4 mb-0 fa-ul text-muted">
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Address: {{ address._Line1 }}&nbsp;{{ address._Line2 }}<br>
		                        	{{ address.cities.name }}&nbsp;{{ address.states.name }}<br>
		                        	India&nbsp;{{ address._Postcode }}</li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone #: {{ address.users._Mobile }}</li>
                      </ul>
                    </div>
                    <div class="col-5 text-center">
                      <img src="assets/uploads/user/{{address.users._Profilepic}}" alt="" class="img-circle img-fluid">
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                  	<button type="button" class="btn btn-sm bg-teal" onclick="ecommerce.showAjaxModal('admin/address/edit/{{address._ID}}','Address Edit', 'modal-lg');"><i class="fa fa-edit"></i> Edit</button>
                  	<button onclick="_sweetalert('{{address._ID}}')" type="button" class="btn btn-sm btn-primary"><i class="fa fa-times"></i> Delete</button>
                  </div>
                </div>
              </div>
            </div>
			{% endfor %}
			{% endif %}

          </div>
        </div>
        <!-- /.card-body -->
        
        <!-- /.card-footer -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>







{% endblock %}

{% block scripts %}
<script>
	function _sweetalert(id) {
		const del_url = base_url+'admin/address/delete/'+id;
		Swal.fire({
		  	title: 'Are you sure?',
		  	text: "You won't be able to revert this!",
		  	icon: 'warning',
		  	showCancelButton: true,
		  	confirmButtonColor: '#3085d6',
		  	cancelButtonColor: '#d33',
		  	confirmButtonText: 'Yes, delete it!',
		  	showLoaderOnConfirm: true,
		  	preConfirm: () => {
		        $.ajax({
	                type: 'POST',
	                url: del_url,
	                success: function(response){
	                	console.log(response)
	                    var temp = JSON.parse(response);
	                    if(temp.type == 'success'){
	                        Swal.fire({
	                            title: temp.msg,
	                            type: "success",
	                            confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
	                            confirmButtonText: 'Ok',
	                            closeOnConfirm: false,
	                            onAfterClose: () => {
									location.reload();    
							  	}
	                        })
	                    }else {
	                        Swal.fire("No records found", '', "error");
	                    }
	                }
	            });
		  	}
		})
	}

	function reloadPage() {
		location.reload();
	}
	{% if this.session.flashdata.type %}
	ecommerce.notifyWithtEle('{{this.session.flashdata.message}}' , '{{this.session.flashdata.type}}' ,'topRight', 2000);
	{% endif %}
</script>
{% endblock %}
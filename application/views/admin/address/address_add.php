<?php
echo form_open(base_url().'admin/address/add', array('class' => 'form','id' => 'validated_form'));
?>
    <div class="row">
        <div class="col-md-6">
            <input type="hidden" name="param" value="<?php echo $param?>">
        	<div class="form-group">
				<label for="addr1" class="control-label">Address Name *</label>
				<input type="text" name="aname" id="aname" class="form-control" placeholder="Address Name" maxlength="50" required/>
			</div>
            <div class="form-group">
                <label for="addr1" class="control-label">Address Line 1</label>
                <input type="text" name="addr1" class="form-control" placeholder="Address Line 1" required/>
            </div>
            <div class="form-group">
                <label for="addr2" class="control-label">Address Line 2</label>
                <input type="text" name="addr2" class="form-control" placeholder="Address Line 2" required/>
            </div>
            <div class="form-group">
                <label for="zip" class="control-label">Zipcode</label>
                <input type="number" name="zip" class="form-control" min="0" placeholder="Zipcode" required/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="city" class="control-label">City</label>
                <input type="text" name="city" class="form-control" placeholder="City" required/>
            </div>
            <div class="form-group">
                <label for="country" class="control-label">Country</label>
                <?php echo form_dropdown('country_id', $country_dd, '','id="country_id" class = "form-control" required');?>
            </div>
            <div class="form-group">
                <label for="state" class="control-label">State</label>
                <?php echo form_dropdown('state_id', array("" => "Select state"), '','id="state_id" class = "form-control" required disabled');?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
    </div>
<?php echo form_close();?>
<script type="text/javascript">a2d3d3J5s7('a2d3d3r7e3s7s7');</script>
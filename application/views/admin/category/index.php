{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

      <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Category</h1>
          </div>
           <div class="col-sm-6">
           <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
           <li class="breadcrumb-item active">category</li>
         </ol>
       </div> 
        </div>
      </div><!-- /.container-fluid -->
    </section>

     <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card card-default">
            <div class="card-header">
        <div class="card-tools">

            <a href="{{base_url('createcategory')}}" class="btn  btn-block bg-gradient-primary pull-right"><i class="fas fa-plus"></i> Add Category</a>

        </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="category_tab" class="table table-bordered table-hover">
                <thead>
                <tr>

                  <th>{% if allcatedata|length > 0 %}
              
            <button class="btn btn-danger" name="delete_all" id="delete_all" class="glyphicon glyphicon-pencil"><i class="fas fa-user-slash"></i></button>
            {% endif %}</th>
                  <th>No</th>
                  <th>Name</th>
                  <th>Description</th> 
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  {% if allcatedata|length > 0 %}
                  {% set i = 1 %}
                    {% for cdata in allcatedata %}
                    
                   
                <tr>
                  <td><input type="checkbox" class="delete_checkbox" name="checkuser[]" id="checkuser" value="{{cdata._ID}}"></td>
                  <td>{{ i }}</td>
                  <td>{{cdata._Name}}</td>
                  <td>{{cdata._Description}}</td>
                  <td>
                    <a href="{{ base_url('editcategory/'~cdata._ID)}}" class="btn btn-warning"><i class="fas fa-edit"></i></a>

                  </td>
                </tr>
                
                  {% set i = i + 1 %}
                 {% endfor %}
                
                             {% endif %}
                </tbody>
              
            </table>

            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

  
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}
   {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/category.js"></script>
   {% endblock %}


{% extends 'admin/app/login_index.php' %}

{% block content %} 

<div class="login-box">
  <div class="login-logo">
    <a href="{{base_url()}}"><b>Admin</b>LTE</a>
  </div>
  <style type="text/css">
    .error-class {
    color:red;  z-index:0; position:relative; display:block; text-align: left; }
  </style>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form id="login-form" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Username" name="username" id="username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div> 
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" name="pass" id="pass">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
          
        <div class="row">
           <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-success float-right">Sign In <i class="fa fa-angle-right ml5"></i></button> 
          </div>
          <!-- /.col -->
        </div>
      </form>

     <!--  <div class="social-auth-links text-center mb-3">
        <p>- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div> -->
      <!-- /.social-auth-links -->

       <p class="mb-1">
        <a href="{{base_url('forgot_pass')}}">I forgot my password</a>
      </p>
     <!--  <p class="mb-0">
       <a href="register.html" class="text-center">Register a new membership</a>
     </p> -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>


{% endblock %}
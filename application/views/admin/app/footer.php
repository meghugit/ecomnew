 <footer class="main-footer">
    <strong>Copyright &copy; 2019-<?php echo date('Y'); ?> <a href="{{base_url('admin/dashboard')}}">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.3-pre
    </div>
  </footer>
  <footer>
   
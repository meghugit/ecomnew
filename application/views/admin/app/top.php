<aside class="main-sidebar sidebar-dark-primary elevation-4">
   <!--  Brand Logo -->
   <a href="{{ base_url('admin/dashboard') }}" class="brand-link">
     <img src="{{ constant('cmstheme') }}dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
          style="opacity: .8">
     <span class="brand-text font-weight-light">AdminLTE 3</span>
   </a> 

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ constant('cmstheme') }}dist/img/AdminLTELogo.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">AdminLTE 3</a>
        </div>
      </div>
       <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview ">
           
            <a href="{{ base_url('admin/dashboard') }}" class="nav-link {% if segment == 'dashboard' %}active {% endif %}">
              <i class="nav-icon fa fa-home"></i>
              <p>
                Dashboard
              </p>
            </a>
           
          </li>
          <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/user') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Customer
              </p>
            </a>
            
          </li>


          <li class="nav-item has-treeview">

            <li class="nav-item has-treeview">

            <a href="{{ base_url('admin/category') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fa fa-sitemap"></i>
              <p>
                Category
              </p>
            </a>
          </li>

             <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/category/subcategory') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fa fa-list-alt"></i>
              <p>
                Subcategory
              </p>
            </a>
            
          </li>



         <!-- <li class="nav-item has-treeview">

            <a href="{{ base_url('admin/attribute') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fa fa-list"></i>
              <p>
                Product Attribute
              </p>
            </a>
            

          </li> -->


             <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/coupon') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fa fa-gift"></i>
              <p>
                Coupons
              </p>
            </a>
            
          </li>

           <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/product') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon  fab fa-palfed"></i>
              <p>
                Product
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/review') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon  fab fa-palfed"></i>
              <p>
                Review
              </p>
            </a>
          </li>

         <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/payment') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fa fa-cogs"></i>
              <p>
                Payment Settings
              </p>
            </a>
          </li>

           <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/wishlist') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fa fa-heart"></i>
              <p>
                Wishlist
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/orders') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fa fa-shopping-cart"></i>
              <p>
                Orders
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/settings') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fa fa-cog"></i>
              <p>
                Settings
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/extrasetting') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fas fa-cogs"></i>
              <p>
                Extra settings
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/store') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fa fa-cog"></i>
              <p>
                Store
              </p>
            </a>
          </li>

           <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/template') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fa fa-file"></i>
              <p>
                Template
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/gallery') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fa fa-picture-o"></i>
              <p>
                Gallery
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview">
            <a href="{{ base_url('admin/pages') }}" class="nav-link {% if segment == 'createhospitalreg' %} active {% endif %} ">
              <i class="nav-icon fa fa-file"></i>
              <p>
                Pages
              </p>
            </a>
          </li>
        </ul>
     </nav>
               <!--- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

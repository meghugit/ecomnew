{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
    	<section class="content-header">
      		<div class="container-fluid">
			<div class="row mb-2">
	          		<div class="col-sm-6">
	             			<h1>Edit Product</h1> 
	          		</div>
		          	<div class="col-sm-6">
			            	<ol class="breadcrumb float-sm-right">
			              		<li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
			              		<li class="breadcrumb-item active">Edit Product</li>
			            	</ol>
		          	</div>
        		</div>
      		</div><!-- /.container-fluid -->
    	</section>

      <!-- Main content -->
    	<section class="content">
      	  <div class="container-fluid">

       	   <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Additional Product Details</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
             <form  id="adddetil_form" name="adddetil_form" method="post" enctype="multipart/form-data">

                <input type="hidden" name="pid" value="{{data[0]._ID}}">
                  {% if data[0]._Adddetails != '' %}
                  
                      {% set i = 0 %}
                      {% for paydata in data[0]._Adddetails['key'] %}
                        <div class="wrapper">
                          <div>
                            <div class="col-md-6 float-left">
                              <div class="form-group">
                                <input type="text" class="form-control " name="key[]"  placeholder="Input key Here" value="{{paydata}}">
                              </div>
                            </div>
                            <div class="col-md-6 float-left">
                              <div class="form-group">
                                <input type="text" class="form-control " name="value[]" placeholder="Input value Here" value="{{data[0]._Adddetails['value'][i]}}">
                              </div>
                            </div>
                          <a href="javascript:void(0);" class="remove_field">Remove</a>
                        </div>
                        </div>

                      {% set i = i + 1 %}

                      {% endfor %}
                  {% else %}
                  
                      <div class="wrapper">
                          <div class="col-md-6 float-left">
                            <div class="form-group">
                              <input type="text" class="form-control " name="key[]" placeholder="Input key Here" />
                            </div>
                          </div>
                          <div class="col-md-6 float-left">
                              <div class="form-group">
                                <input type="text" class="form-control" name="value[]" placeholder="Input value Here" />
                              </div>
                            </div>
                          </div>
                      </div>

                  {% endif %}
                  <p><button class="add_fields btn btn-info">Add More Fields</button></p>

                  <input type="submit" class="btn btn-success" value="save" style="margin-top: 10px;">

              <a href="{{base_url('editproduct/')~data[0]._ID}}" class="btn btn-dark" style="float: right;">back</a>
              </form>

              </div>
         <!-- /.form -->
          </div>
          <!-- /.card-body -->
        </div>
      </div>
        <!-- /.card -->    


      
        <!-- /.row -->            	
  	</section>
</div>
  <!-- /.content-wrapper -->

  {% endblock %}

  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/product.js"></script>

    <script type="text/javascript">

        $(document).ready(function() {
            var wrapper    = $(".wrapper"); 
            var add_button = $(".add_fields"); 
            var x = 1; 

            $(add_button).click(function(e){
                e.preventDefault();
      
                $('div.wrapper:last').append('<div> <div class="col-md-6 float-left"><div class="form-group"><input type="text" name="key[]" placeholder="Input key Here" class="form-control float-left"/></div></div> <div class="col-md-6 float-left"><div class="form-group"><input type="text" name="value[]" placeholder="Input value Here" class="form-control  "/></div></div> <a href="javascript:void(0);" class="remove_field">Remove</a></div>');
            });

                $(wrapper).on("click",".remove_field", function(e){ 
                    e.preventDefault();
                    $(this).parent('div').remove(); 
                    x--; 
                })
            });

    </script>
   {% endblock %}



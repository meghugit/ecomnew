{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

      <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Subcategory</h1>
          </div>
           <div class="col-sm-6">
           <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
           <li class="breadcrumb-item active">subcategory</li>
         </ol>
       </div> 
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card card-default">
              <div class="card-header">
                <div class="card-tools">
                  <a href="{{base_url('createsubcategory')}}" class="btn  btn-block bg-gradient-primary pull-right"><i class="fas fa-plus"></i> Add Sub Category</a>

                </div>
              </div>
              <div class="card-body">
                 <table id="scategory_tab" class="table table-bordered table-hover">
                  <thead>
                  <tr>

                    <th>{% if subcatedata|length > 0 %}
              
            <button class="btn btn-danger" name="delete_all" id="delete_all" class="glyphicon glyphicon-pencil"><i class="fas fa-user-slash"></i></button>
            {% endif %}</th>

                    <th>No</th>
                    <th>Name</th>
                    <th>Description</th> 
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>

                    {% if subcatedata|length > 0 %}
                    {% set i = 1 %}
                      {% for sdata in subcatedata %}
                      
                  <tr>

                     <td><input type="checkbox" class="delete_checkbox" name="checkuser[]" id="checkuser" value="{{sdata._ID}}"></td>
                    <td>{{ i }}</td>
                    <td>{{sdata._Name}}</td>
                    <td>{{sdata._Description}}</td>
                   
                    <td> 
                       <a href="{{ base_url('editsubcategory/'~sdata._ID) }}" class="btn btn-warning"><i class="fas fa-edit"></i></a>

                    </td>
                  </tr>
                  
                    {% set i = i + 1 %}
                   {% endfor %}
                  
                 {% endif %}
                  </tbody>
                
               </table>
                
            </div>
            <!-- /.card-body -->
          </div>
      
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->

        

    </div>
</section>

  </div>
  <!-- /.content-wrapper -->

  {% endblock %}
{% block scripts %}
  <script src="{{ constant('cmstheme') }}js/subcategory.js"></script>
   {% endblock %}
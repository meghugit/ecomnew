{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    	<!-- Content Header (Page header) -->
    	<section class="content-header">
      		<div class="container-fluid">
			<div class="row mb-2">
	          		<div class="col-sm-6">
	             			<h1>{{pagetitle}}</h1> 
	          		</div>
		          	<div class="col-sm-6">
			            	<ol class="breadcrumb float-sm-right">
			              		<li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
			              		<li class="breadcrumb-item active">category</li>
			            	</ol>
		          	</div>
        		</div>
      		</div><!-- /.container-fluid -->
    	</section>

      <!-- Main content -->
    	<section class="content">
      		<div class="row">
        		<div class="col-12">
          			<div class="card">
            				<div class="card-body">
               					<div class="row">
              						<div class="col-md-12">              
              							<form class="form-horizontal"  id="field_form" name="field_form" method="post">
                						{% if editid is not defined %}
						                   	<div class="form-group row">
						                    		<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Title1</label>
						                    		<div class="col-sm-11">
						                    			<input type="text"  class="form-control" id="title1" name="title1" placeholder="Enter Title" >
						                    		</div>
						                  	</div>
                   							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Title2</label>
							                    	<div class="col-sm-11">
							                    		<input type="text"  class="form-control" id="title2" name="title2" placeholder="Enter Title" >
							                    	</div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Image</label>
							                    	<div class="col-sm-11">
								                    	<input type="file" name="img_name" id="img_name">
							                    	</div>
                  							</div>
                  							<input type="submit" class="btn btn-success" value="Submit" style="margin-top: 10px;">
							        {% else %}  
							        {% if editfield|length > 0 %}

          								<input type="hidden" id="hfid" name="hfid" value="{{editfield._ID}}">
          								<input type="hidden" id="himg" name="himg" value="{{editfield._Image}}">
                 							<div class="form-group row">
						                    		<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Title1</label>
						                    		<div class="col-sm-11">
						                    			<input type="text"  class="form-control" id="title1" name="title1" placeholder="Enter Title" value="{{editfield._Title1}}" >
						                    		</div>
						                  	</div>
                   							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Title2</label>
							                    	<div class="col-sm-11">
							                    		<input type="text"  class="form-control" id="title2" name="title2" placeholder="Enter Title" value="{{editfield._Title2}}" >
							                    	</div>
                  							</div>
                  							<div class="form-group row">
							                    	<label class="col-sm-1 col-form-label" for="exampleInputEmail1">Image</label>
							                    	<div class="col-sm-11">
								                    	<input type="file" name="img_name" id="img_name">
								                    	<img src="{{base_url()}}assets/uploads/setting/{{editfield._Image}}" width="250px" height="100px">
							                    	</div>
                  							</div>
          								<input type="submit" class="btn btn-success" value="Update" style="margin-top: 10px;">
	                  					{% endif %}
                  						{% endif %}
      								</form>
                  					</div>
                  				</div>
                  			</div>
                  		</div>
                  	</div>
		</div>                  	
  	</section>
</div>
  <!-- /.content-wrapper -->

  {% endblock %}

{% block scripts %}
	<script src="{{ constant('cmstheme') }}js/esetting.js"></script>
  {% endblock %}
{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
             <h1>Review</h1> 
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
              <li class="breadcrumb-item active">Review</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
             <div class="card-header">
                <div class="card-tools">
                 <!--  <a href="{{base_url('addproduct')}}" class="btn  btn-block bg-gradient-primary pull-right"><i class="fas fa-plus"></i> Add Product</a>
                                 </div> -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              
            
              <table id="review_tab" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Title</th>
                  <th>Review</th>
                  <th>Rate</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  {% if reviewdata|length > 0 %}

                
                  {% set i = 1 %}
                    {% for rdata in reviewdata %}
                    
                <tr>
                  <td>{{ i }}</td>
                  <td>{{rdata._Title}}</td>
                  <td>{{rdata._Review}}</td>
                  <td>{{rdata._Rate}}</td>
                  <td> 

                    <a href="{{ base_url('viewreview/'~rdata._ID)}}" class="btn btn-primary btn-sm">view</a> 
                    {% if url == 'review' %}
                   
                   <a {% if rdata._Status == 1 %} class="btn btn-success btn-sm disabled" style="cursor:not-allowed;" {% else %} href="{{ base_url('proapprovereview/'~rdata._ID)}}" class="btn btn-success btn-sm"{% endif %}>{% if rdata._Status ==1 %}Approved {% else %}Appprove {% endif %}</a>  
                   
                   <a {% if rdata._Status == 0 %} class="btn btn-danger btn-sm disabled" style="cursor:not-allowed;"{% else %} href="{{ base_url('prorejectreview/'~rdata._ID)}}" class="btn btn-danger btn-sm"{% endif %}>{% if rdata._Status ==0 %}Rejected {% else %}Reject {% endif %}</a>
                   {% else %}                   

                   <a {% if rdata._Status == 1 %} class="btn btn-success btn-sm disabled" style="cursor:not-allowed;" {% else %} href="{{ base_url('approvereview/'~rdata._ID)}}" class="btn btn-success btn-sm"{% endif %}>{% if rdata._Status ==1 %}Approved {% else %}Appprove {% endif %}</a>  
                   
                   <a {% if rdata._Status == 0 %} class="btn btn-danger btn-sm disabled" style="cursor:not-allowed;"{% else %} href="{{ base_url('rejectview/'~rdata._ID)}}" class="btn btn-danger btn-sm"{% endif %}>{% if rdata._Status ==0 %}Rejected {% else %}Reject {% endif %}</a>

                  {% endif %}

                  </td>
                </tr>
                
                  {% set i = i + 1 %}
                 {% endfor %}
                
               {% endif %}
                </tbody>
              
            </table>

            </div>
            <!-- /.card-body -->
          </div>
        
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}


  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/product.js"></script>
   {% endblock %}



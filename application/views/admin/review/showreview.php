{% extends 'admin/app/index.php' %}

{% block content %}

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

      <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Review Details</h1>
          </div>
           <div class="col-sm-6">
           <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{base_url('admin/dashboard')}}">Dashboard</a></li>
           <li class="breadcrumb-item active">Review</li>
         </ol>
       </div> 
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card  card-default">

            <div class="card-body">
              <form  id="review_form" method="post">
              <table class="table">
                {% if count(reviewdetail) > 0 %}
                 <tr>
                   <td>Title : </td> 
                  <td>{{reviewdetail._Title}}</td>
                </tr>
                 
                   
                <tr>
                   <td>Description: </td> 
                  <td>{{reviewdetail._Description}}</td>
                </tr>
                <tr>
                  <td>Review</td>
                   <td>{{reviewdetail._Review}}</td>

                   </td>
                </tr>
                 <tr>
                  <td>Rate</td>
                   <td><h5><span class="badge badge-success">{{reviewdetail._Rate}} <i class="fa fa-star" aria-hidden="true"></i></h5></span></td>
                </tr>
                 <tr>
                  <td>User</td>
                   <td>{{reviewdetail['users'].fullname}}</td>
                </tr>
                <tr>
                  <td>product</td>
                   <td>{{reviewdetail['products']._Name}}</td>
                </tr> 
                <tr>
                  <td>status</td>
                   <td>{% if(reviewdetail._Status==0) %}Rejected
                    {% else %}
                    Approved
                      {%endif %}
                    
                   </td>
                </tr>
               {% endif %}
              </table>
                 </form>
            </div>
            
            
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

  
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
</section>
 
  </div>
  <!-- /.content-wrapper -->

  {% endblock %}

  {% block scripts %}
  <script src="{{ constant('cmstheme') }}js/order.js"></script>
    {% endblock %}

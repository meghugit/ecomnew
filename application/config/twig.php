<?php

$config['twig']['template_dir'] = VIEWPATH;
$config['twig']['template_ext'] = 'php';
$config['twig']['environment']['autoescape'] = TRUE;
$config['twig']['environment']['cache'] = FALSE;
$config['twig']['environment']['debug'] = FALSE;
$config['twig']['environment']['strict_variables'] = FALSE;

$config['twig']['register_functions'] = array
(
    // Some functions from the url_helper.php
    'site_url', 'base_url', 'uri_string', 'anchor', 'url_title', 'get_cookie', 'set_cookie','cart_view_header', 'cart_view_count'
);

$config['twig']['register_filters'] = array
(
    // Functions (filters) from the inflector_helper.php
    'singular', 'plural', 'camelize', 'underscore', 'humanize'
);
?>
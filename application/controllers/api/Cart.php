<?php

use chriskacerguis\RestServer\RestController;

class Cart extends RestController {

    public function __construct() {
       	parent::__construct();
    }

    /**********Cart Details using Login Id**********/
    public function cartView_get($loguser_id = '0'){

        $cart_data = Carts1::with('product')
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();

        $addetail = Stores::where('_ID',1)
                            ->first()
                            ->toarray();
        
        $now = date('Y-m-d');
        $coupon_data = Couponcode::whereDate('_Fromdate','<=',$now)
                                ->whereDate('_Todate','>=',$now)
                                ->get()
                                ->toArray();
        
        $j = 0;
        $yes_cat = 0;
        $dis_price = 0;
        $other_price = 0;
        $final_dis_price = 0;
        $discount_offer = 0;
        $total_price = 0;

        for($i = 0;$i < count($cart_data) ; $i++){
        
            if(in_array($cart_data[$i]['product']['_SubcatID'],$coupon_data[$j]['_Subcatid'])){
                $yes_cat = $yes_cat+1;
            }
        }

        for($i = 0;$i < count($cart_data) ; $i++){
            $total_price +=  $cart_data[$i]['_Subtotal'];
        }
       
       if($yes_cat == count($coupon_data[$j]['_Subcatid'])){
           for($i = 0;$i < count($cart_data) ; $i++){
             
                if(in_array($cart_data[$i]['product']['_SubcatID'],$coupon_data[$j]['_Subcatid'])){
                    $dis_price += $cart_data[$i]['_Unitprice'];
                    $old_q = $cart_data[$i]['_Quantity'] - 1;
                    if($old_q != '0'){
                        $other_price += $cart_data[$i]['_Unitprice'] * $old_q;
                    }
                }
                else{
                    $other_price += $cart_data[$i]['_Subtotal'];
                }
            }
        }

        if($yes_cat == count($coupon_data[$j]['_Subcatid'])){
            $discount_offer = 1;
            $final_dis_price = $dis_price - $coupon_data[0]['_Amount'];
        }
        else{
            for($i = 0;$i < count($cart_data) ; $i++){
                $other_price += $cart_data[$i]['_Subtotal'];
            }
        }
        
        $final_price = $total_price - $final_dis_price;
        $allpage  = Page::get()
                        ->toarray();

        $res                        = [];
        $data_new                   = [];
        $data_new['cart_data']      = $cart_data;
        $data_new['addetail']       = $addetail;
        $data_new['coupon_data']    = $coupon_data;
        $data_new['loguser_id']     = $loguser_id;
        $data_new['final_price']    = $final_price;
        $data_new['discount_offer'] = $discount_offer;
        $data_new['allpage']        = $allpage;
     
        array_push($res, $data_new);
        $this->response($res, RestController::HTTP_OK);
    }

    /********Add to cart functionality ************/
    public function addtocart_post()
    {
        $res        = [];
        $post       = $this->request->body;
        $userid     = $post['userid'];
        $product_id = $post['product_id'];
        $size_val   = $post['size_val'];
        $color_val  = $post['color_val'];
        $quantity   = $post['quantity'];

        $p_data = Products::where('_ID',$product_id)
                            ->get()
                            ->toArray();
        
        $cart_check = Carts1::where('_ProductID',$product_id)
                            ->where('_UserID',$userid)
                            ->get()
                            ->toArray();

        $att_data_fetch = Attdetail::where('_ProID',$p_data[0]['_ID'])
                                    ->where('_Default','1')
                                    ->get()
                                    ->toArray();

        if($att_data_fetch[0]['_Sellprice'] != ''){
            $price = $att_data_fetch[0]['_Sellprice'];    
        }
        else{
            $price = $att_data_fetch[0]['_Price'];
        }

        if($post){
            $qty    = $quantity;
            $stotal = $price * $qty;
        }
        else{
            $qty    = '1';
            $stotal = $price;
        }
       
        if($cart_check){
            $find_id =  $find_id=$cart_check[0]['_UserID'];
            $upd_cart  = Carts1::where('_UserID',$find_id)
                                ->where('_ProductID',$product_id)
                                ->where('_Size',$size_val)
                                ->where('_Color',$color_val)
                                ->first(); 
            if($upd_cart){
                $upd_cart->_UserID    = $userid;
                $upd_cart->_Quantity  = $qty;
                $upd_cart->_Unitprice = $price;
                $upd_cart->_Subtotal  = $stotal;
                $upd_cart->_Size      = $size_val;
                $upd_cart->_Color     = $color_val;
                $upd_cart->_Created   = date("Y-m-d H:i:s");
                $upd_cart->save();    
            }
            else{
                            
                $upd_cart1     = Carts1::where('_UserID',$userid)
                                        ->where('_ProductID',$product_id)
                                        ->where('_Size',$size_val)
                                        ->where('_Color',$color_val)
                                        ->first();

                if($upd_cart1){

                    $upd_cart1->_UserID    = $userid;
                    $upd_cart1->_Quantity  = $qty;
                    $upd_cart1->_Unitprice = $price;
                    $upd_cart1->_Subtotal  = $stotal;
                    $upd_cart1->_Size      = $size_val;
                    $upd_cart1->_Color     = $color_val;
                    $upd_cart1->_Created   = date("Y-m-d H:i:s");
                    $upd_cart1->save();
                }
                else{
                    $ins_cart             = new Carts1();
                    $ins_cart->_ProductID = $product_id;
                    $ins_cart->_UserID    = $userid;
                    $ins_cart->_Quantity  = $qty;
                    $ins_cart->_Unitprice = $price;
                    $ins_cart->_Subtotal  = $stotal;
                    $ins_cart->_Size      = $size_val;
                    $ins_cart->_Color     = $color_val;
                    $ins_cart->_Created   = date("Y-m-d H:i:s");
                    $ins_cart->save();
                }
            }
        }
        else{
            $ins_cart             = new Carts1();
            $ins_cart->_ProductID = $product_id;
            $ins_cart->_UserID    = $userid;
            $ins_cart->_Quantity  = $qty;
            $ins_cart->_Unitprice = $price;
            $ins_cart->_Subtotal  = $stotal;
            $ins_cart->_Size      = $size_val;
            $ins_cart->_Color     = $color_val;
            $ins_cart->_Created   = date("Y-m-d H:i:s");
            $ins_cart->save();
        }
        $res = ['type' => 'success' , 
                'msg' => 'Add to Cart successfully ',
                'result' => true];
        
        $this->response($res, RestController::HTTP_OK);
    } 

    /**********Remove Cart functionality***********/
    public function removeCart_delete($id){
        $del = Carts1::where('_ID',$id)->delete();
        $this->response(['Cart deleted successfully.'], RestController::HTTP_OK);
    }

}
<?php

use chriskacerguis\RestServer\RestController;

class Wishlist extends RestController {

    public function __construct() {
       	parent::__construct();
    }

    /*********Get User Wishlist Data *************/
    public function getWishlist_get($id = ''){
        $res= [];
        
        $res['wishdata'] = Wishlists::with('products', 'products.reviews')
                            ->where('_Userid',$id)
                            ->get()
                            ->toarray();
        
        $res['addetail'] = Stores::where('_ID',1)
                            ->first()
                            ->toarray();

        $res['allpage']  = Page::get()->toarray();
        $this->response($res, RestController::HTTP_OK);
    }

    /************Add Wishlist functionality************/
    public function addWishlist_post(){

        $res        = [];
        $post       = $this->request->body;
        $userid     = $post['userid'];
        $productid  = $post['productid'];
        
        $wishdata             = new Wishlists();
        $wishdata->_Userid    = $userid;
        $wishdata->_Productid = $productid;
        $wishdata->_Created   = date("Y-m-d H:i:s");
        $wishdata->save();

        $res = ['type' => 'success' , 
                'msg' => 'Add Wishlist Successfully.',
                'result' => true];
        
        $this->response($res, RestController::HTTP_OK);
    }

    /***********Remove User Wishlist ***************/
    public function removeWishlist_delete($id = ""){
        $del = Wishlists::where('_ID',$id)->delete();

        $res = ['type' => 'success' ,
                'msg' => 'Wishlists deleted Successfully',
                'result' => true];

        $this->response($res , RestController::HTTP_OK);
    }

}
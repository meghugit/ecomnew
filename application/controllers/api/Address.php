<?php

use chriskacerguis\RestServer\RestController;

class Address extends RestController {

    public function __construct() {
       	parent::__construct();
    }

    /*******Add address functionality************/
    public function addAddress_post(){

        $post   = $this->request->body;
        $userid = $post['userid'];
        $save   = $post['other'];

        if($save=='save' && $save!='')
        {
            $aname = isset($post['aname']) ? $post['aname'] : null;
            $addr1 = isset($post['addr1']) ? $post['addr1'] : null;
            $addr2 = isset($post['addr2']) ? $post['addr2'] : null;
            $zip   = isset($post['zip']) ? $post['zip'] : null;
            $city  = isset($post['city']) ? $post['city'] : null;
            $state = isset($post['state']) ? $post['state'] : null;
         
            $othadd            = new Addresses();
            $othadd->_Name     = $aname;
            $othadd->_UserID   = $userid;
            $othadd->_Line1    = $addr1;
            $othadd->_Line2    = $addr2;
            $othadd->_Postcode = $zip;
            $othadd->_City     = $city;
            $othadd->_State    = $state;
            $othadd->save();
            $res = array('result' => "success", 
                         "type" => "success", 
                         "msg" => "Address saved successfully");
        }
        else
        {
            $res = array('result' => "success", 
                         "type" => "success",
                         "msg" => "Address Fetch successfully");
        }
        $this->response($res , RestController::HTTP_OK);
    }

    /*********Fetch address at edit time functionality *************/
    public function geteditAddr_get($id = '0'){
        
        $res = [];
        if($id){
            $res['address'] = Addresses::where('_ID',$id)
                                        ->first()
                                        ->toArray();
        }else{
            $res['address'] = [];
        }
        $res['allcity'] = Cities::get()->toarray();
        $res['allstate'] = States::get()->toarray();
        $res['id'] = $id;
        $this->response($res , RestController::HTTP_OK);
    }

    /*********Update Address information **************/
    public function updateAddr_post()
    {
        $post   = $this->request->body;
        $userid = $post['userid'];
        $id     = $post['id'];

        $aname = $post['aname'] ? $post['aname'] : null;
        $addr1 = $post['addr1'] ? $post['addr1'] : null;
        $addr2 = $post['addr2'] ? $post['addr2'] : null;
        $zip   = $post['zip'] ? $post['zip'] : null;
        $city  = $post['city'] ? $post['city'] : null;
        $state = $post['state'] ? $post['state'] : null;

        if($id){
            $address = Addresses::find($id);
        }else{
            $address = new Addresses();
        }

        $address->_Name     = $aname;
        $address->_UserID   = $userid;
        $address->_Line1    = $addr1;
        $address->_Line2    = $addr2;
        $address->_Postcode = $zip;
        $address->_City     = $city;
        $address->_State    = $state;
        $address->save();

        if($id){
           $res = ['type' => 'success' ,
                   'msg' => 'Address Updated Successfully',
                   'result' => true];
        }else{
            $res = ['type' => 'success' ,
                   'msg' => 'Address Inserted Successfully',
                   'result' => true];
        }
        $this->response($res , RestController::HTTP_OK);
    }

    /********Fetch Address Using Login ID***********/
    public function fetchAddr_get($id = ''){
        $res= [];
        
        $res['custadd'] = Addresses::with('users','cities','states')
                            ->where('_UserID',$id)
                            ->get()
                            ->toarray();

        $res['addetail'] = Stores::where('_ID',1)
                            ->first()
                            ->toarray();

        $res['allcity']  = Cities::get()->toarray();
        $res['allstate'] = States::get()->toarray();
        $res['allpage']  = Page::get()->toarray();

        $this->response($res , RestController::HTTP_OK);
    }

    /***********Remove User Address***************/
    public function removeAddr_delete($id = ""){
        $del = Addresses::where('_ID',$id)->delete();

        $res = ['type' => 'success' ,
                'msg' => 'Address deleted Successfully',
                'result' => true];

        $this->response($res , RestController::HTTP_OK);
    }

    /****Set Address (Payment Method )********/
    public function setAddress_get($loguser_id = ''){
        $res= [];
        $res['loguser_id'] = $loguser_id;

        $res['cart_data'] = Carts1::with('product','user')
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();
        $res['allProd'] = [];
        $sum = 0;

        foreach ($res['cart_data'] as  $value) {
            $sum = $value['_Subtotal'] + $sum;
            $res['allProd']['total']=$sum;
        }

        $res['paydata'] = Payments::get()->toarray();
        $res['allpage'] = Page::get()->toarray();

        $res['new_order_data'] = Order::with('coupon')
                                    ->where('_Userid',$loguser_id)
                                    ->where('_Orderstatus','0')
                                    ->get()
                                    ->toArray();
        
        $res['addetail'] = Stores::where('_ID',1)
                                ->first()
                                ->toarray();
        $this->response($res, RestController::HTTP_OK);
    }

}
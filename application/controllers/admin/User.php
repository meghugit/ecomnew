<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}

    }

    public function index()
	{
		$userdata = Users::with('Cities')->get()->toarray();
    $this->load->view('admin/user/alluser',compact('userdata'));
	}


	public function deluser(){
    if($this->input->post('id'))
  {
    $checkbox_value = $this->input->post('id');
    for($count = 0; $count < count($checkbox_value); $count++)
   {
		$userdel = Users::where('_ID',$checkbox_value[$count])->first()->toArray();
		Users::find($userdel['_ID'])->delete();
    if($userdel['_Profilepic']!='')
    {  
		unlink('assets/uploads/user/'.$userdel['_Profilepic']);
    }
  }
   $res = ['type' => 'success' , 'msg' => 'User Detail Deleted successfully ','url'=>'admin/user', 'result' => true];
    echo json_encode($res);
    exit;
   
  }


	}

	public function show($id = "")
	{
		$customer = Users::find($id);
		$this->load->view('admin/user/customer_view',compact('customer'));
	}


	public function edituser($id){
		$allcity = Cities::get()->toarray();
		$allstate = States::get()->toarray();
		$userinfo = Users::where('_ID',$id)->first()->toarray();
		$this->load->view('admin/user/edituser',compact('userinfo','allstate','allcity'));
	}

	public function getcity()
	{
		$stateid = $this->input->post('id');
		$citidata = Cities::where('state_id',$stateid)
					->orderBy('name', 'ASC')
					->get()
					->toarray();
		echo json_encode($citidata);
	}

	public function userupdate()
	{
		 $huserid =  $this->input->post('huserid');
		 $usertab  = Users::find($huserid);
     $fname =  $this->input->post('fname');
     $lname =  $this->input->post('lname');
     $email =  $this->input->post('email');
     $mno =  $this->input->post('mno');
     $password =  md5($this->input->post('password'));
     $gender =  $this->input->post('gender');

    
     $config['upload_path'] = "assets/uploads/user";
     $config['allowed_types'] = 'gif|jpg|png';
     $this->load->library('upload');
     $this->upload->initialize($config);

     if (!$this->upload->do_upload('user_photo')) {
        $error = array('error' => $this->upload->display_errors());
    } else {
      
      $data = $this->upload->data();
        if(!empty($data['file_name']))
        {
            $usertab->_Profilepic = $data['file_name'];
           
        }
        else
        {
          $usertab->_Profilepic =$usertab->_Profilepic;
          
        }
    }
   
      $usertab->_Firstname =  $fname;
      $usertab->_Lastname = $lname;
      $usertab->_Email = $email;
      $usertab->_Mobile = $mno;
      $usertab->_Password = $password;
      $usertab->_Gender = $gender;
      $usertab->_Created = date("Y-m-d H:i:s");
      $usertab->save();

     $res = ['type' => 'success' , 'msg' => 'User Detail Updated successfully ','url'=>'admin/user', 'result' => true];
    echo json_encode($res);
    exit;
	}


	public function authentication_login(){
		$log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        if($log_id > 0){
			return $log_id;
        }
       return 0;
    }
}
?>   
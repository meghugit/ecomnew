<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Store extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}
    }

    public function authentication_login(){
        $log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        if($log_id > 0){
            return $log_id;
        }
       return 0;
    }

    public function index()
	{
        $allstore = Stores::where('_ID',1)->first();
		$this->load->view('admin/store/index',compact('allstore'));
	}

    public function updatestore(){
       $stname =  $this->input->post('stname');
       $des =  $this->input->post('des');
       $email =  $this->input->post('email');
       $mno = $this->input->post('mno');
       $add =  $this->input->post('add');
       $hsid =  $this->input->post('hsid');
       $sinfo =Stores::where('_ID',$hsid)->first();
       $path =  "assets/uploads/store";
        $config['upload_path']   = $path;
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']      = 10000000;
       $this->load->library('upload');
       $this->upload->initialize($config);
       
        if (!$this->upload->do_upload('storeimag')) {
            $error = array('error' => $this->upload->display_errors());
        } 
        else {
            $data = $this->upload->data();
            if(!empty($data['file_name']))
            {
                $sinfo->_Logo = $data['file_name'];
               
            }
            else
            {
              $sinfo->_Logo =$sinfo->_Logo;
              
            }
        }
        $sinfo->_Name = $stname;
        $sinfo->_Des = $des;
        $sinfo->_Email = $email;
        $sinfo->_mobile = $mno;
        $sinfo->_Address = $add;
        $sinfo->_Created = date('Y-m-d H:i:s');
        $sinfo->save();
         $res = ['type' => 'success' , 'msg' => 'Store Updated successfully ','url'=>'admin/store', 'result' => true];
        echo json_encode($res);
        exit;

    }
    public function dellogo(){
      $id = $this->input->post('id');
       $allstore = Stores::where('_ID',$id)->first();
        unlink('assets/uploads/store/'.$allstore->_Logo);
        $allstore->_Logo = '';
       $allstore->save();
        return redirect('admin/store');
       
    }

   
	
   
}
?>   
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}
    }

    public function index()
	{
		$allpayment =  Payments::all();
		$this->load->view('admin/payment/index',compact('allpayment'));
	}

	public function createpayment(){
		$editid = $this->uri->segment(2);
		if($editid)
		{
		  $editcat =  Payments::where("_ID",$editid)->first();
		  $this->load->view('admin/payment/payment_add',compact('editid',compact('editcat')));
		}
		else
		{	
		  $this->load->view('admin/payment/payment_add');
		}
	}

	public function addpayment(){
		$payname = $this->input->post('payname');
		$paydes = $this->input->post('paydes');

		$exits_pay = Payments::where('_Name', $this->input->post('payname'))
			->get()->toarray();

		if($exits_pay){
			$res = ['type' => 'warning' , 'msg' => 'Payment Already Exist.','url'=>'admin/payment', 'result' => true];
	        echo json_encode($res);
	        exit;
		}
		else{
			$paytab  = new Payments();
	        $paytab->_Name = $payname ;
	        $paytab->_Description = $paydes ;
	        $paytab->_Created  = date('Y-m-d H:i:s');
	        $paytab->save();

	        $res = ['type' => 'success' , 'msg' => 'Payment created successfully ','url'=>'admin/payment', 'result' => true];
	        echo json_encode($res);
	        exit;
		}
	}


	public function delpayment(){
        if($this->input->post('id'))
  		{
	    	$checkbox_value = $this->input->post('id');
		    for($count = 0; $count < count($checkbox_value); $count++)
		   {
				$affectedRows = Payments::where('_ID',$checkbox_value[$count])->first()->toArray();
				Payments::find($affectedRows['_ID'])->delete();
		   }
		   if($affectedRows == true){
				$res = ['type' => 'success' , 'msg' => 'Payment deleted successfully ','url'=>'admin/Payment', 'result' => true];
			}else{
				$res = ['type' => 'error' , 'msg' => 'Payment not deleted/ ','url'=>'admin/Payment', 'result' => true];
			}
			echo json_encode($res);
	        	exit;
  		}
	}

	public function editpayment($id){
		$editpay =  Payments::where("_ID",$id)->first();
		$editid = $id;
		$this->load->view('admin/payment/payment_add',compact('editpay','editid'));
	}

	public function updatepayment(){
		$hpayid = $this->input->post('hpayid');
		$payname = $this->input->post('payname');
		$paydes = $this->input->post('paydes');

		$exits_pay = Payments::where('_Name', $this->input->post('payname'))
								->where('_ID','!=',$hpayid)
								->get()->toarray();

		if($exits_pay){
			$res = ['type' => 'warning' , 'msg' => 'Payment Already Exist.','url'=>'admin/payment', 'result' => true];
	        echo json_encode($res);
	        exit;
		}
		else{
			$payresult = Payments::where("_ID",$hpayid)->first();
			$payresult->_Name = $payname;
			$payresult->_Description = $paydes ;
	        $payresult->_Created  = date('Y-m-d H:i:s');
	        $payresult->save();

	        $res = ['type' => 'success' , 'msg' => 'Payment updated successfully ','url'=>'admin/payment', 'result' => true];
	        echo json_encode($res);
	        exit;
	    }
	}

	public function paymentdetails($id){
		$data = Payments::where('_ID',$id)->get()->toArray();
		$this->load->view('admin/payment/paymentdetails',compact('data'));
	}

	public function addpdetail_form(){
		$new_key = array_filter($this->input->post('key'));
		$new_val = array_filter($this->input->post('value'));

		$data = ["key" => $new_key,"value" => $new_val];

		$up_details = Payments::find($this->input->post('pid'));
		$up_details->_Details = $data;
		$up_details->save();

		$res = ['type' => 'success' , 'msg' => 'Payment Details Added successfully ','url'=>'admin/payment', 'result' => true];
	        echo json_encode($res);
	        exit;
	}

	public function verifypayment(){
		
		if($this->input->post('_payname') == $this->input->post('payname')){
			echo 'true';
		}else {
			
			$c = Payments::where('_Name', $this->input->post('payname'))
			->get()->toarray();
			if(count($c)> 0)
			{
				echo 'false';
			}
			else
			{
				echo 'true';
			}
		}
	}


	public function authentication_login(){
		$log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        if($log_id > 0){
			return $log_id;
        }
       return 0;
    }
}
?>   
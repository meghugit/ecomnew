<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends CI_Controller {

	function __construct()
	{
		parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}
	}

	public function addr_list($param = "")
	{
		$addresses = Addresses::with('cities', 'states','users')
					->where('_UserID',$param)
					->get()
					->toArray();
		$this->session->set_userdata('address_user', $param);
		$this->load->view('admin/address/index', compact('addresses'));
	}

	
	// DECLARATION: ADD ADDRESS
	function create($param = "")
	{
		$address = Addresses::with('cities', 'states')->get()->toarray();
		$this->load->view('admin/address/address_add',compact('address'));
	}

	public function edit($param1 = "")
	{
		if($param1){
			$address = Addresses::where('_ID',$param1)->first()->toArray();
		}else{
			$address = [];
		}
		$allcity = Cities::get()->toarray();
		$allstate = States::get()->toarray();
		$this->load->view('admin/address/edit',compact('address','allstate','allcity', 'param1'));
	}

	// DECLARATION: UPDATE ADDRESS
	public function update()
	{
		$userid = $this->input->post('userid', null);
		$id = $this->input->post('id', null);
		$name = $this->input->post('aname', null);
		$addr1 = $this->input->post('addr1', null);
		$addr2 = $this->input->post('addr2', null);
		$zip = $this->input->post('zip', null);
		$city = $this->input->post('city', null);
		$state = $this->input->post('state', null);

		if($id){
			$address = Addresses::find($id);
		}else{
			$address = new Addresses();
		}

		$address->_Name = $name;
		$address->_UserID = $userid;
		$address->_Line1 = $addr1;
		$address->_Line2 = $addr2;
		$address->_Postcode = $zip;
		$address->_City = $city;
		$address->_State = $state;
		$address->save();

		if($id){
			$this->session->set_flashdata('message', 'Address Updated Successfully');
			$this->session->set_flashdata('type', 'success');
		}else{
			$this->session->set_flashdata('message', 'Address Inserted Successfully');
			$this->session->set_flashdata('type', 'success');
		}
		redirect(base_url() . 'address/'.$userid,'refresh');
	}

	// DECLARATION: DELETE ADDRESS
	public function delete($param1 = "")
	{
		$address = Addresses::where('_ID',$param1)->first()->toArray();
		Addresses::find($address['_ID'])->delete();
		$data = ['type' => 'success','msg' => 'Address deleted successfully'];
		echo json_encode($data);
		exit;
	}

	public function authentication_login(){
		$log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        if($log_id > 0){
			return $log_id;
        }
       return 0;
    }

}
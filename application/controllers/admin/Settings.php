<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}
    }

    public function authentication_login(){
        $log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        if($log_id > 0){
            return $log_id;
        }
       return 0;
    }

    public function index()
	{
		$general_data = Setting::where('_Status','0')->get()->all();
        $social_data = Setting::where('_Status','1')->get()->all();
		$this->load->view('admin/settings/index',compact('general_data','social_data'));
	}

    public function genderal_settings(){
      
        for($i = 0 ;$i < count($_POST['key']); $i++){
            $upd1 = Setting::where('_Key','=',$_POST['key'][$i])->first();
            if($upd1){
                $upd1->_Value=$_POST['value'][$i];
                $upd1->save();
            }
            else{
                $ins = new Setting();
                $ins->_Key = $_POST['key'][$i];
                $ins->_Value= $_POST['value'][$i];
                $ins->save();
            }
        }

        $all_data = Setting::where('_Status','0')->get();

        foreach ($all_data as $key => $value) {
            if(!in_array($value['_Key'],$_POST['key'])){
                $del = Setting::where('_Key',$value['_Key'])->delete();
            }
        }

        $res = ['type' => 'success' , 'msg' => 'General Settings Updated successfully ','url'=>'admin/settings', 'result' => true];
        echo json_encode($res);
        exit;
      
    }

    public function social_settings(){

        foreach ($_POST as $key => $value) {
            $upd1 = Setting::where('_Key','=',$key)->first();
            $upd1->_Value=$value;
            $upd1->save();
        }

        $res = ['type' => 'success' , 'msg' => 'Social Settings Updated successfully ','url'=>'admin/settings', 'result' => true];
        echo json_encode($res);
        exit;

    }
	
   
}
?>   
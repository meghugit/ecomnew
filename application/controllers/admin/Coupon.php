<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}

    }

    public function index()
	{
		$allcoupon = Couponcode::get()->toarray();
		$this->load->view('admin/coupon/allcoupon',compact('allcoupon'));
	}

	public function createcoupon()
	{
		$editcouponid = $this->uri->segment(2);
		$category = Categories::where('_Main_id','0')->get()->toarray();
		if($editcouponid)
		{

		  $this->load->view('admin/coupon/coupon_add',compact('editcouponid','category'));
		}
		else
		{/*echo "hello";exit;*/	

		  $this->load->view('admin/coupon/coupon_add',compact('category'));
		}
	}

	public function addcoupon()
	{

		$code = $this->input->post('code');
		$des  = $this->input->post('coupondes');
		$type = $this->input->post('coupontype');
		$amount = $this->input->post('amount');
		$fdate = $this->input->post('fromdate');
		$tdate = $this->input->post('todate');
		$catid= $this->input->post('category');
		$subcatid= $this->input->post('subcategory[]');
		$status = $this->input->post('status');
		$s = (($status=='on')?1:0);
		$coupontab  = new Couponcode();

		$path =  "assets/uploads/coupon";
        $config['upload_path']   = $path;
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size']      = 10000000;
       $this->load->library('upload');
       $this->upload->initialize($config);

        if (!$this->upload->do_upload('couponimage')) {
            $error = array('error' => $this->upload->display_errors());
        } 
        else {
            $data = $this->upload->data();
            $coupontab->_Image = $data['file_name'];
        }

		$coupontab->_Code = $code;
		$coupontab->_Description = $des;
		$coupontab->_Type = $type;
		$coupontab->_Amount = $amount;
		$coupontab->_Fromdate = date('Y-m-d',strtotime($fdate));
		$coupontab->_Todate = date('Y-m-d',strtotime($tdate));
		$coupontab->_CatID = $catid;
		$coupontab->_Subcatid = $subcatid;
		$coupontab->_Status = $s;
		$coupontab->_Created = date("Y-m-d H:i:s");
		$coupontab->save();
		
		$res = ['type' => 'success' , 'msg' => 'Coupon Created successfully ','url'=>'admin/coupon', 'result' => true];
	    echo json_encode($res);
	    exit;
	}


	public function delcoupon(){
     	if($this->input->post('id'))
  		{
		    $checkbox_value = $this->input->post('id');
		    for($count = 0; $count < count($checkbox_value); $count++)
		   {
				$couondel = Couponcode::where('_ID',$checkbox_value[$count])->first()->toArray();
				Couponcode::find($couondel['_ID'])->delete();
			    if($couondel['_Image']!='')
			    {  
					unlink('assets/uploads/coupon/'.$couondel['_Image']);
		    	}
		  }
		   $res = ['type' => 'success' , 'msg' => 'Coupon Deleted successfully ','url'=>'admin/coupon', 'result' => true];
		    echo json_encode($res);
		    exit;
       }
	}

	public function editcoupon($id){
		$editcoupondata =  Couponcode::where("_ID",$id)->first()->toarray();
		$category = Categories::where('_Main_id','0')->get()->toarray();
		$subcategory = Categories::where('_Main_id',$editcoupondata['_CatID'])->get()->toArray();

		$editcouponid = $id;
		$this->load->view('admin/coupon/coupon_add',compact('editcoupondata','editcouponid','category','subcategory'));

	}

	public function updatecoupon(){
		$hcouponid   = $this->input->post('hcouponid');
		$code = $this->input->post('code');
		$des  = $this->input->post('coupondes');
		$type = $this->input->post('coupontype');
		$amount = $this->input->post('amount');
		$fdate = $this->input->post('fromdate');
		$tdate = $this->input->post('todate');
		$catid= $this->input->post('category');
		$subcatid= $this->input->post('subcategory[]');
		$status = $this->input->post('status');
		$s = (($status=='on')?1:0);
		$editcoudata =  Couponcode::where("_ID",$hcouponid)->first();

		$path =  "assets/uploads/coupon";
        $config['upload_path']   = $path;
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['max_size']      = 10000000;
       $this->load->library('upload');
       $this->upload->initialize($config);

        if (!$this->upload->do_upload('couponimage')) {
            $error = array('error' => $this->upload->display_errors());
        } 
        else {
        $data = $this->upload->data();
        if(!empty($data['file_name']))
        {
          $editcoudata->_Image = $data['file_name'];
           
        }
        else
        {
          $editcoudata->_Image = $editcoudata->_Image;
          
        }
        }

		$editcoudata->_Code = $code;
		$editcoudata->_Description = $des;
		$editcoudata->_Type = $type;
		$editcoudata->_Amount = $amount;
		$editcoudata->_Fromdate = date('Y-m-d',strtotime($fdate));
		$editcoudata->_Todate = date('Y-m-d',strtotime($tdate));
		$editcoudata->_CatID = $catid;
		$editcoudata->_Subcatid = $subcatid;
		$editcoudata->_Status = $s;
		$editcoudata->_Created = date("Y-m-d H:i:s");
		$editcoudata->save();

		$res = ['type' => 'success' , 'msg' => 'Coupon Updated successfully ','url'=>'admin/coupon', 'result' => true];
	    echo json_encode($res);
	    exit;
	}


	 public function authentication_login(){
		$log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        if($log_id > 0){
			return $log_id;
        }
       return 0;
    }
}
?>   
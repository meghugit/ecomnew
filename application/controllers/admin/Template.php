<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
		if($this->authentication_login() === 0){
			return redirect('admin');
		}

    }
       public function authentication_login(){
        $log_id=$this->encryption->decrypt($this->input->cookie('adminid'));
        if($log_id > 0){
            return $log_id;
        }
       return 0;
    }

    	public function index()
	{
		$tempall = Templates::get()->toarray();
		$this->load->view('admin/template/index',compact('tempall'));
	}
	public function createtemplate()
	{

		$editid = $this->uri->segment(2);
        if($editid) {
            $edittemp = Templates::where("_ID",$editid)->first();
            $pagetitle = "Edit Template";
            $this->load->view('admin/template/edit',compact('editid', 'pagetitle','edittemp'));
        }
        else
        {
            $pagetitle = "Add Template";
            $this->load->view('admin/template/edit',compact('pagetitle'));
        }
	}

	public function addtemplate(){
		$name = $this->input->post('name');
		$subject = $this->input->post('subject');
		$content = $this->input->post('content');
		$mail = $this->input->post('mailto');
		$type = $this->input->post('type[]');
		$temtab = new Templates();
		$temtab->_Name = $name;
		$temtab->_Subject = $subject;
		$temtab->_Slug = str_replace(' ', '-', strtolower($name));
		$temtab->_Content = $content;
		$temtab->_Mail_to = $mail;
		$temtab->_Type = $type;
		$temtab->_Created = date('Y-m-d H:i:s');
		$temtab->save();
		$res = ['type' => 'success' , 'msg' => 'Template created successfully ','url'=>'admin/template', 'result' => true];
        echo json_encode($res);
        exit;
	}

	public function updatetemplate(){

		$htid = $this->input->post('htid');
		$name = $this->input->post('name');
		$slug = $this->input->post('slug');
		$subject = $this->input->post('subject');
		$content = $this->input->post('content');
		$mail = $this->input->post('mailto');
		$type = $this->input->post('type[]');
		$edittemp =Templates::where('_ID',$htid)->first();
		$edittemp->_Name = $name;
		$edittemp->_Subject = $subject;
		$edittemp->_Slug = $slug;
		$edittemp->_Content = $content;
		$edittemp->_Mail_to = $mail;
		$edittemp->_Type = $type;
		$edittemp->_Created = date('Y-m-d H:i:s');
		$edittemp->save();
		$res = ['type' => 'success' , 'msg' => 'Template Updated successfully ','url'=>'admin/template', 'result' => true];
        echo json_encode($res);
        exit;

	}

	public function deltempate(){
         if($this->input->post('id'))
        {
            $checkbox_value = $this->input->post('id');
            for($count = 0; $count < count($checkbox_value); $count++)
           {
           		$affectedRows = Templates::where('_ID',$checkbox_value[$count])->first()->toArray();
               Templates::find($affectedRows['_ID'])->delete();
           }
           if($affectedRows == true){
                        $res = ['type' => 'success' , 'msg' => 'Template deleted successfully ','url'=>'admin/template', 'result' => true];
                    }else{
                        $res = ['type' => 'error' , 'msg' => 'Template not deleted/ ','url'=>'admin/template', 'result' => true];
                    }
                    echo json_encode($res);
                        exit;
        }

	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	
    public function __construct(){

    	parent::__construct();
    	
    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		  $this->output->set_header('Pragma: no-cache');
    }

    public function index(){
     
	}

    public function product_detail($id){

        $loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));
        /*$tt = cart_view();*/
          /*echo '<pre>';print_r($cart_data_new);exit;*/
        $fetch_data = Products::where('_ID',$id)
                                ->get()
                                ->toArray();

        $adet_data = Attdetail::where('_ProID',$fetch_data['0']['_ID'])
                                ->get()
                                ->toArray();

        $attr_data = Attributes::get()->all();

        $cart_data = Carts1::where('_ProductID',$id)
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();
       
        $allreview = Reviews::with('users')->where('_ProductID',$id)->where('_Status','1')->get()->toarray();
        

       $addetail = Stores::where('_ID',1)->first()->toarray();
       $allpage  = Page::get()->toarray();

        $this->load->view('front/product/product_detail',compact('id','fetch_data','attr_data','adet_data','allreview','cart_data','addetail','allpage'));
    }

    public function all_search_action(){
        $search = $this->input->post('search');

        $cat_data = Categories::where('_Main_id','0')->get()->all();
        $category = Categories::get()->toArray();

        $cat_search = Categories::where('_Name',$search)
                                ->get()
                                ->toArray();

        if(count($cat_search) > 0){
            $sid = $cat_search[0]['_ID'];
            $products = Products::where('_CatID',$sid)    
                        ->orWhere('_SubcatID',$sid)
                        ->orWhere('_Name',$search)
                        ->get()
                        ->toArray();
        }
        else{
            $products = Products::where('_Name','like','%'.$search.'%')
                                ->get()
                                ->toArray();
        }
                              
        $addetail = Stores::where('_ID',1)->first()->toarray();
        $allpage  = Page::get()->toarray();
        $this->load->view('front/shop/productlist',compact('products', 'addetail','cat_data','category','allpage'));
       
    }

    public function quick_view($id){

        if($this->input->cookie('userid')){
            $loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));
        }
        elseif($this->input->cookie('userid_new')){
            $loguser_id=$this->input->cookie('userid_new');   
        }
        else{
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 
                
                    $input_length = strlen($permitted_chars);
                    $random_string = '';
                    for($i = 0; $i < 20; $i++) {
                        $random_character = $permitted_chars[mt_rand(0, $input_length - 1)];
                        $random_string .= $random_character;
                    }

            $loguser_id = $random_string;
            /*set_cookie('userid', $loguser_id,  time()+86400);*/
           /*set_cookie('userid',$loguser_id,time() + 24 * 3600);*/
            set_cookie('userid_new',$loguser_id,time() + 24 * 3600);
        }


        /*$tt = cart_view();*/
          /*echo '<pre>';print_r($cart_data_new);exit;*/
        $fetch_data = Products::where('_ID',$id)
                                ->get()
                                ->toArray();

        $adet_data = Attdetail::where('_ProID',$fetch_data['0']['_ID'])
                                ->get()
                                ->toArray();

        $cart_data = Carts1::where('_ProductID',$id)
                            ->where('_UserID',$loguser_id)
                            ->get()
                            ->toArray();
        /*echo '<pre>';print_r($cart_data);exit;*/

        $attr_data = Attributes::get()->all();

        $this->load->view('front/product/quick_view',compact('id','fetch_data','attr_data','adet_data','cart_data'));
    }

 /*   public function get_proprice(){
        $val =  $this->input->post('id');
        $p_id =  $this->input->post('p_id');
        /*$result = Attdetail::whereJsonContains('_Attinfo[id]',$val)->get()->toArray();
        $result = Attdetail::where('_ProID',$p_id)
                            ->whereRaw('JSON_CONTAINS(_Attinfo,\'{"id":"'.$val.'"}\')')
                            ->get()
                            ->toArray();
        echo json_encode($result);
    }*/

    public function add_user_wishlist($id){
        $loguser_id=$this->encryption->decrypt($this->input->cookie('userid'));
        
        $ins_wish             = new Wishlists();
        $ins_wish->_Userid    = $loguser_id;
        $ins_wish->_Productid = $id;
        $ins_wish->_Created   = date("Y-m-d H:i:s");
        $ins_wish->save();

        $this->load->view('front/wishlist');
    }

    public function reviewform_action(){
        $title  = $this->input->post('title');
        $review = $this->input->post('review');
        $star   = $this->input->post('vote-price');
        $pid    = $this->input->post('pr_id');
        $uid    = $this->encryption->decrypt($this->input->cookie('userid'));

        $check_review = Reviews::where('_Productid',$pid)
                                ->where('_Userid',$uid)
                                ->get()
                                ->toArray();

        if($check_review){
            $upd_review = Reviews::find($check_review[0]['_ID']);
            $upd_review->_Title     = $title;
            $upd_review->_Review    = $review;
            $upd_review->_Rate      = $star;
            $upd_review->_ProductID = $pid;
            $upd_review->_UserID    = $uid;
            $upd_review->_Status    = '0';
            $upd_review->save();
        }
        else{
            $ins_review             = new Reviews();
            $ins_review->_Title     = $title;
            $ins_review->_Review    = $review;
            $ins_review->_Rate      = $star;
            $ins_review->_ProductID = $pid;
            $ins_review->_UserID    = $uid;
            $ins_review->_Status    = '0';
            $ins_review->_Created   = date("Y-m-d H:i:s");
            $ins_review->save();
        }
        $proid = "product_detail/$pid";
        $res = ['type' => 'success' , 'msg' => 'Review Added successfully ','url'=>$proid, 'result' => true];
        echo json_encode($res);
        exit;
    }

    public function productlist(){

        $pro_data = Products::whereHas('att_detail_func',function($q){
            $q->where('_Default','1');
        })->get()->all();

        $pro_data = Attdetail::where('')

        echo '<pre>';print_r($pro_data);exit;

        $search = $this->input->get('search');
        $cat_data = Categories::where('_Main_id','0')->get()->all();
        $category = Categories::get()->toArray();
        
        if($search){
            $cat_search = Categories::where('_Name',$search)
                                ->get()
                                ->toArray();

            if(count($cat_search) > 0){
                $sid = $cat_search[0]['_ID'];
                $products = Products::where('_CatID',$sid)    
                            ->orWhere('_SubcatID',$sid)
                            ->orWhere('_Name',$search)
                            ->get()
                            ->toArray();
            }
            else{
                $products = Products::where('_Name','like','%'.$search.'%')
                                    ->get()
                                    ->toArray();
            }
            $addetail = Stores::where('_ID',1)->first()->toarray();
        }
        else{
            $products = Products::with('wishlists')
                                ->where('_CatID',$category)
                                ->orderBy('_ID', 'DESC')
                                ->get()
                                ->toArray();

            $addetail = Stores::where('_ID',1)->first()->toarray();
        }

        $this->load->view('front/shop/productlist',compact('products', 'addetail','cat_data','category'));

        $addetail = Stores::where('_ID',1)->first()->toarray();
        $allpage  = Page::get()->toarray();
        $this->load->view('front/shop/productlist',compact('products', 'addetail','cat_data','category','allpage'));

       
    } 

    public function pro($id){


        $cat_data = Categories::where('_Main_id','0')->get()->all();

        $category = categories::get()->toArray();

        $products = Products::with('wishlists')
                                ->orderBy('_ID', 'DESC')
                                ->where('_SubcatID',$id)
                                ->get()
                                ->toArray();
        $addetail = Stores::where('_ID',1)->first()->toarray();
        $allpage  = Page::get()->toarray();

        $this->load->view('front/shop/productlist',compact('products','cat_data', 'addetail','category','allpage'));
       
    }

    public function getsize()
    {
        $cat = $this->input->post('catid');
        if($cat)
        {
            $find_data = Attributes::where(function($find_data) use ($cat){
                for($i = 0 ;$i < count($cat) ; $i++){
                    $find_data->orWhere('_Subcat',$cat[$i]);
                }
            });
             $data = $find_data->get()->toArray();

            /* $res = ['type' => 'success' , 'msg' => 'Review Added successfully ','url'=>'productlist', 
             'result' => true,'data' =>$data];*/
             echo json_encode($data);
        }
    }

    public function search_size_pro()
    {
        $param = $this->input->post('param');
        $attid = $this->input->post('id');
        //$find_size->Where('_AttID',$attid)
        //
        if($param)
        {
               $find_size = Attdetail::with('product')->where(function($find_size) use ($param,$attid){
                for($i = 0 ;$i < count($param) ; $i++){
                    
                   $find_size->whereJsonContains('_Attinfo', ['id' => $param[$i]]);
                  /* ->whereRaw('JSON_EXTRACT(`_Attinfo` , "$[*].quantity") !=', "");*/
                 
                }
            });





            $data = $find_size->get()->toArray();
            /*echo '<pre>';
               print_r($data);
               exit;*/
            echo json_encode($data);
        }
    }

public function search_prolist_filter(){
     $cat = $this->input->post('category');
       if($cat){
        $find_data = Products::where(function($find_data) use ($cat){
                for($i = 0 ;$i < count($cat) ; $i++){
                    $find_data->orWhere('_SubcatID',$cat[$i]);
                }
            });
           /* $find_data = Products::where('_SubcatID',$cat)->get()->toarray();*/
            $data = $find_data->get()->toArray();
             echo json_encode($data);
        }
    
}
public function filterprice_product(){
    $cat = $this->input->post('category');
    $min = $this->input->post('minprice');
    $max = $this->input->post('maxprice');
   
    if($cat)
    {
        $price_data = Products::where('_SubcatID',$cat)
        ->whereBetween('_Price',[intval($min),intval($max)])
        ->get()->toArray();
        echo json_encode($price_data);
        exit;
    }


}

}	


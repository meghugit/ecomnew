<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	


    public function __construct(){

    	parent::__construct();
    	
    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		  $this->output->set_header('Pragma: no-cache');
    }

    public function index(){
    	/*$featured_prodata = Products::with('wishlists')
    							->where('_Status', '=', '1')
                                ->take('8')
                                ->orderBy('_Created', 'DESC')
                                ->get()
                                ->toArray();
        foreach ($featured_prodata as $key => $value) {
        	$featuredProd[$key] = $value;
        	$featuredProd[$key]['class'] = 'feature';
        }
        
        $sale_prodata = Products::with('wishlists')
    							->where('_Status', '=', '1')
                                ->take('4')
                                ->get()
                                ->toArray();
		foreach ($sale_prodata as $key => $value) {
			$saleProd[$key] = $value;
        	$saleProd[$key]['class'] = 'discount';
        }

		$popular_data = Products::with('reviews')
							->where('_Special','1')
							->where('_Status', '=', '1')
                            ->take('4')
                            ->get()
                            ->toArray();
        foreach ($popular_data as $key => $value) {
        	$popularProd[$key] = $value;
        	$popularProd[$key]['class'] = 'popular';
        }
        
        $products = array_merge($featuredProd, $saleProd, $popularProd);
        $addetail = Stores::where('_ID',1)->first()->toarray();

        $userid_new = $this->input->cookie('userid_new');
        
        
          $log_id = $this->encryption->decrypt($this->input->cookie('userid'));
          if($log_id)
          {
            Carts1::where('_UserID', $userid_new)->get()
        ->each(function ($img) use ($log_id) {
             $img->_UserID = $log_id;
             $img->save();
         });
          }

          $extdata = Extrafield::get()->toarray();
          $allpage  = Page::get()->toarray();
        
      
		$this->load->view('front/index',compact('products', 'addetail','extdata','log_id','allpage'));*/

    /*  $featured_prodata = Products::with('wishlists')
                  ->where('_Status', '=', '1')
                                ->take('8')
                                ->orderBy('_Created', 'DESC')
                                ->get()
                                ->toArray();*/

      $featured_prodata =Attdetail::with(['product.wishlists','product' => function ($query) {
    $query->where('_Status', '=', '1');


   
   
}])->where('_Default', '=', 1) 
->take('8')
->orderBy('_Created', 'DESC')->get()->toArray();

/*echo '<pre>';
print_r($featured_prodata);*/
     
         foreach ($featured_prodata as $key => $value) {
          $featuredProd[$key] = $value;
           if($value['product']['is_new']==1)
          {
          $featuredProd[$key]['class'] = 'new';
        }
      }
/*echo '<pre>';
print_r($featuredProd);exit;*/
       
       /* echo '<pre>';
print_r($featuredProd);exit;*/
/* $query->where('is_new', '=', '1');*/

/* $sale_prodata  =Products::with(['wishlists','Attdetail' => function ($query) {
    $query->where('_Default', '=', 1);
    $query->where('_Sellprice', '>', '0');
   
}])->where('_Status', '=', '1') 
->take('4')
->get()->toArray();
*/
                 

                 
                
         $sale_prodata = Attdetail::with(['product.wishlists','product' => function ($query) {
          $query->where('_Status', '=', '1');
          /*$query->where('is_sale', '=', '1');*/
         }])
         ->where('_Default','=',1)
         ->where('_Sellprice', '>', '0')
         ->take('4')
         ->get()
         ->toArray();
        
    
        foreach ($sale_prodata as $key => $value) {
            $saleProd[$key] = $value;
             if($value['product']['is_sale']==1)
          {
            $saleProd[$key]['class'] = 'sale';
          }
        }

       /* echo '<pre>';
print_r($saleProd);
exit;*/
    
        
        $products = array_merge($featuredProd, $saleProd);
        $addetail = Stores::where('_ID',1)->first()->toarray();

        $userid_new = $this->input->cookie('userid_new');
        
        
          $log_id = $this->encryption->decrypt($this->input->cookie('userid'));
          if($log_id)
          {
            Carts1::where('_UserID', $userid_new)->get()
        ->each(function ($img) use ($log_id) {
             $img->_UserID = $log_id;
             $img->save();
         });
          }
         

          $extdata = Extrafield::get()->toarray();
          $allpage  = Page::get()->toarray();
        
      
    $this->load->view('front/index',compact('saleProd','products', 'addetail','extdata','log_id','allpage'));
	  }

    public function login(){
    $page = Page::where('_ID',$id)->first()->toarray();
    $addetail = Stores::where('_ID',1)->first()->toarray();
      $this->load->view('front/login',compact('page','allpage'));
    }

    public function addwishlist($param = "")
    {
       $log_id = $this->encryption->decrypt($this->input->cookie('userid'));
       /*$pdata = Products::where('_ID',$param)->first(); */
       $wishdata = new Wishlists();
       $wishdata->_Userid = $log_id;
       $wishdata->_Productid = $param;
       $wishdata->_Created = date("Y-m-d H:i:s");
       $wishdata->save();
       redirect('showwishlist');
      // $this->load->view('');

    }

    public function showpages($id = ""){
    
    $page = Page::where('_ID',$id)->where('_Status',1)->first()->toarray();
    $addetail = Stores::where('_ID',1)->first()->toarray();
    $this->load->view('front/pages/showpages',compact('page','allpage'));
  }


}	


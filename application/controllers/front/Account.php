<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct(){

    	parent::__construct();

    	$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		$this->output->set_header('Pragma: no-cache');
    }
 
	public function Profile(){
		$pagename = "My Profile";
		$id = $this->encryption->decrypt($this->input->cookie('userid'));
		$userdetail = Users::where("_ID",$id)->first()->toarray();
		$statedata = States::get()->toarray();
		$addetail = Stores::where('_ID',1)->first()->toarray();
		$allpage  = Page::get()->toarray();
		$this->load->view('front/account/profile',compact('pagename','userdetail','statedata','addetail','allpage'));
	}

	public function getallcity()
	{
		$stateid = $this->input->post('id');
		$citidata = Cities::where('state_id',$stateid)->get()->toarray();
		echo json_encode($citidata);
	}

	public function profile_action(){
		$prohid = $this->input->post('prohid');
		$usertab  = Users::find($prohid);
		$fname = $this->input->post('fname');
		$lname = $this->input->post('lname');
		$email = $this->input->post('email');
		$mno = $this->input->post('mno');
		$gender = $this->input->post('gender');
		$config['upload_path'] = "assets/uploads/user";
	    $config['allowed_types'] = 'gif|jpg|png';
	    $this->load->library('upload');
	    $this->upload->initialize($config);

	     if (!$this->upload->do_upload('user_photo')) {
	        $error = array('error' => $this->upload->display_errors());
	    } else {
	      
	      $imgdata = $this->upload->data();

	        if(!empty($imgdata['file_name']))


	        {
	            $usertab->_Profilepic = $imgdata['file_name'];
	           
	        }
	        else
	        {
	          $usertab->_Profilepic =$usertab->_Profilepic;
	          
	        }
	    }
	  $usertab->_Firstname =  $fname;
      $usertab->_Lastname = $lname;
      $usertab->_Email = $email;
      $usertab->_Mobile = $mno;
      $usertab->_Gender = $gender;
      $usertab->_Created = date("Y-m-d H:i:s");
      $usertab->save();

     $res = ['type' => 'success' , 'msg' => 'User Detail Updated successfully ','url'=>'userprofile', 'result' => true];
    echo json_encode($res);
    exit;

	}

	public function propass_action(){
		$prohid = $this->input->post('prohid');
		$cpass = md5($this->input->post('cpass'));
		$newpass = $this->input->post('newpass');
		$editusertab  = Users::where('_Password',$cpass)->first();
		$editusertab->_Password = md5($newpass);
		$editusertab->_Created = date("Y-m-d H:i:s");
      	$editusertab->save();
   		 $res = ['type' => 'success' , 'msg' => 'Password Updated successfully ','url'=>'userprofile', 'result' => true];
    	echo json_encode($res);
    	exit;
	}

	public function showwishlist(){
		$id = $this->encryption->decrypt($this->input->cookie('userid'));
		$wishdata = Wishlists::with('products', 'products.reviews')->where('_Userid',$id)->get()->toarray();
		
		$addetail = Stores::where('_ID',1)->first()->toarray();
		$allpage  = Page::get()->toarray();
		$this->load->view('front/account/wishlist',compact('wishdata','addetail','allpage'));
	}

	public function remove_wishlist($id){
		 $del = Wishlists::where('_ID',$id)->delete();

        redirect('showwishlist');

	}

	public function showorders(){
		$id = $this->encryption->decrypt($this->input->cookie('userid'));
		$orderdata = Users::with('order')->where('_ID',$id)->first()->toarray();
		$addetail = Stores::where('_ID',1)->first()->toarray();
		$allpage  = Page::get()->toarray();

		$this->load->view('front/account/orders',compact('orderdata','addetail','allpage'));
	}

	public function orderdetail($id)
	{

		$orprodetail = Order::with('product')->where("_ID",$id)->first()->toarray();
		$coupon_data = Couponcode::get()->toArray();
		/*echo "<pre>";
		print_r($orprodetail);
		exit;*/
		$addetail = Stores::where('_ID',1)->first()->toarray();
		$allpage  = Page::get()->toarray();
		$this->load->view('front/account/orderdetail',compact('orprodetail','addetail','coupon_data','allpage'));

	}

	public function customeraddress()
	{
		$id = $this->encryption->decrypt($this->input->cookie('userid'));
		$custadd = Addresses::with('users','cities','states')->where('_UserID',$id)->get()->toarray();
		$addetail = Stores::where('_ID',1)->first()->toarray();
		$allpage  = Page::get()->toarray();
		$this->load->view('front/account/customeraddress',compact('custadd','addetail','allpage'));
	}

	public function adddelete($param1 = ""){
		$address = Addresses::where('_ID',$param1)->first()->toArray();
		Addresses::find($address['_ID'])->delete();
		$data = ['type' => 'success','msg' => 'Address deleted successfully'];
		echo json_encode($data);
		exit;

	}

	public function edit($param1 = "")
	{
		$id = $this->encryption->decrypt($this->input->cookie('userid'));
		if($param1){
			$address = Addresses::where('_ID',$param1)->first()->toArray();
		}else{
			$address = [];
		}
		$allcity = Cities::get()->toarray();
		$allstate = States::get()->toarray();
		
		$this->load->view('front/account/address_edit',compact('address','allstate','allcity', 'param1','id'));
	}

	// DECLARATION: UPDATE ADDRESS
	public function update()
	{
		$userid = $this->input->post('userid', null);
		$id = $this->input->post('id', null);
		$name = $this->input->post('aname', null);
		$addr1 = $this->input->post('addr1', null);
		$addr2 = $this->input->post('addr2', null);
		$zip = $this->input->post('zip', null);
		$city = $this->input->post('city', null);
		$state = $this->input->post('state', null);

		if($id){
			$address = Addresses::find($id);
		}else{
			$address = new Addresses();
		}

		$address->_Name = $name;
		$address->_UserID = $userid;
		$address->_Line1 = $addr1;
		$address->_Line2 = $addr2;
		$address->_Postcode = $zip;
		$address->_City = $city;
		$address->_State = $state;
		$address->save();

		if($id){
			$this->session->set_flashdata('message', 'Address Updated Successfully');
			$this->session->set_flashdata('type', 'success');
		}else{
			$this->session->set_flashdata('message', 'Address Inserted Successfully');
			$this->session->set_flashdata('type', 'success');
		}
		redirect(base_url() . 'customeraddress','refresh');
	}

	
}
?>
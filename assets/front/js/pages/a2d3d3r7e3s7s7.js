$(function() {
    
    /* Get Base Url */
    var base_url = document.getElementById("myBase").href;
    /* End Get Base Url */
    
    
    $("#editAddressform").validate({
            rules: {
                aname: {
                    required: true,
                },
                addr1: {
                    required:true,
                },
                addr2: {
                  required:true,
                },
                zip: {
                    required:true,
                },
                state:{
                  required:true,
                },
                city:{
                  required:true,
                }
            },
            messages: {
                aname: {
                    required: "Please Enter Address.",
                },
                addr1: {
                    required:"Please Enter Address.",
                },
                addr2: {
                  required:"Please Enter Address.",
                },
                zip: {
                    required:"Please Enter Zipcode.",
                },
                state:{
                  required:"Please Select State.",
                },
                city:{
                  required:"Please Select City."
                  ,
                }
            },
            errorClass: "error-class",
            errorElement: "div",
            errorPlacement: function(error, element) {
                    if(element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
           unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
            
        });

});
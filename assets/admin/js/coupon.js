var base_url = $("#myBase").attr('href');

$(function() {
    coupon.init();
    
}); 


coupon = {
  init:function () {
    coupon._extrafunction();
    coupon._couponcode();
    
  },
    _extrafunction:function(){
     
  $('#picker1,#picker2').datetimepicker({
        format: "DD-MM-YYYY"
    })
    $(document).ready(function () {

        bsCustomFileInput.init();
        $('#delete_all').click(function(){
      var checkbox = $('.delete_checkbox:checked');
  
      if(checkbox.length > 0)
      {
       var checkbox_value = [];
       $(checkbox).each(function(){
        checkbox_value.push($(this).val());
       });
     
       Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!',
              showLoaderOnConfirm: true,
              preConfirm: () => {
                    $.ajax({
                            type: 'POST',
                            url: base_url+'delcoupon',
                            data:{id:checkbox_value},
                            success: function(response){
                                var temp = JSON.parse(response);
                                    if(temp.type == 'success'){
                                        Swal.fire({
                                                title: temp.msg,
                                                type: "success",
                                                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                                                confirmButtonText: 'Ok',
                                                closeOnConfirm: false,
                                                onAfterClose:()=>{
                                                  redirecturl(temp.url)
                                                }
                                        })
                                    }else {
                                        Swal.fire("No records found", '', "error");
                                    }

                            }

                        });
              }
        }) ;
   function  redirecturl(url =""){
                 window.location.href = url;
              }

  }
  else
  {
   alert('Select atleast one records');
  }

 });
    })

    $(document).on('change', '#category', function () {
       
        var cat_id = $(this).val();
        coupon.ajax_req({id: cat_id},"getsubcategory").done(function (response) {
            var res = $.parseJSON(response);

            $('#subcat_options').empty();
            var cat_html = '';

                        if(res.length >0){
                            for(var i=0 ; i<res.length ; i++){
                                cat_html += '<option value="'+res[i]._ID+'">'+res[i]._Name+'</option>';
                            }
                        }

            $("#sub_category").show();
            console.log(cat_html);
            $("#subcat_options").html(cat_html);
        
         })
    })

   

},

  notifyWithtEle: function (msg,type,pos,timeout) {
        pos = "";
    timeout = "";
        var noty = new Noty({
      theme:'metroui',
      text: msg,
      type: type,
      layout: (pos != "") ? pos : 'topRight',
      timeout: (timeout != "") ? timeout : 2000,
      closeWith: ['click'],
      animation: {
        open: 'animated slideInRight',
        close: 'animated slideOutRight'
      }

    });
        noty.show();
    },

    ajax_req: function(fields, url) {
      return $.ajax({
          url:base_url+url,
          type:'POST',
          data: fields,
          datatype : "application/json"
      });
    },
   
    _couponcode : function () {
       jQuery.validator.addMethod("greaterThan", 
function(value, element, params) {

    if (!/Invalid|NaN/.test(new Date(value))) {
        return new Date(value) > new Date($(params).val());
    }

    return isNaN(value) && isNaN($(params).val()) 
        || (Number(value) > Number($(params).val())); 
},'Must be greater than From Date.');
        $("#coupon_form").validate({
            rules: {
                code: {
                    required: true,   
                },   
                coupondes: {
                    required:true,
                },
                coupontype:{
                    required:true,
                },
                amount :{
                    required:true,
                    digits:true,
                },
                fromdate: {
                    required :true,
                },
                todate: {
                    required :true,
                    //greaterThan: "#fromdate"
                },     
                "category[]":{
                    required :true,
                }
            },
            messages: {
                code: {
                  required: "Please Enter Code",
                },
                coupondes: {
                    required :"Please Enter Description",
                },
                coupontype:{
                    required:"Please Enter Coupon Type",
                },
                amount: {
                    required:"Please Enter Amount",
                    digits:"Please Enter Only Number",
                },
                fromdate:{
                    required :"Please Select From Date",
                },
                todate :{
                    required :"Please Select To Date",
                },
                "category[]":{
                    required :"Please Select Category",
                }
               
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                console.log(element);

                if(element.prop( "class" ) === "form-control datetimepicker-input"){
                  error.insertAfter( element.parent( "div" ) );
                } 
                else {
                  error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
                var file_data = $('#couponimage').prop('files')[0];
                var form_data = new FormData($('#coupon_form')[0]);
                form_data.append('file', file_data);
                var hcouponid = $("#hcouponid").val();
                if(hcouponid)
                {
                    $.ajax({
                            url: base_url + 'updatecoupon', // point to server-side controller method
                           datatype : "application/json", // what to expect back from the server
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                            var res = $.parseJSON(response);
                            if(res.type == 'success'){
                                coupon.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                window.location = base_url+res.url;
                            }else if(res.type == 'warning'){
                                coupon.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                window.location = base_url+res.url;
                            }else{
                                coupon.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            }
                            },
                            
                        });
                    return false;
                }
                else
                {
                    $.ajax({
                            url: base_url + 'addcoupon', // point to server-side controller method
                           datatype : "application/json", // what to expect back from the server
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                            var res = $.parseJSON(response);
                            if(res.type == 'success'){
                                coupon.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                window.location = base_url+res.url;
                            }else if(res.type == 'warning'){
                                coupon.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                window.location = base_url+res.url;
                            }else{
                                coupon.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            }
                            },
                            
                        });
                    return false;
                }
            }
        })
    },

}
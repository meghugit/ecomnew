var base_url = $("#myBase").attr('href');

$(function() {
    pages.init();
    
}); 


pages = {

  init:function () {

    pages._extrafunction();
    pages._addpages();
   
  },
    _extrafunction:function(){
    $(document).ready(function () {
        bsCustomFileInput.init();
        $('#delete_all').click(function(){
      var checkbox = $('.delete_checkbox:checked');
  
      if(checkbox.length > 0)
      {
       var checkbox_value = [];
       $(checkbox).each(function(){
        checkbox_value.push($(this).val());
       });
     
       Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!',
              showLoaderOnConfirm: true,
              preConfirm: () => {
                    $.ajax({
                            type: 'POST',
                            url: base_url+'delpages',
                            data:{id:checkbox_value},
                            success: function(response){
                                var temp = JSON.parse(response);
                                    if(temp.type == 'success'){
                                        Swal.fire({
                                                title: temp.msg,
                                                type: "success",
                                                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                                                confirmButtonText: 'Ok',
                                                closeOnConfirm: false,
                                                onAfterClose:()=>{
                                                  redirecturl(temp.url)
                                                }
                                        })
                                    }else {
                                        Swal.fire("No records found", '', "error");
                                    }

                            }

                        });
              }
        }) ;
   function  redirecturl(url =""){
                 window.location.href = url;
              }

  }
  else
  {
   alert('Select atleast one records');
  }

 });
    })

    },

    
  notifyWithtEle: function (msg,type,pos,timeout) {
        pos = "";
    timeout = "";
        var noty = new Noty({
      theme:'metroui',
      text: msg,
      type: type,
      layout: (pos != "") ? pos : 'topRight',
      timeout: (timeout != "") ? timeout : 2000,
      closeWith: ['click'],
      animation: {
        open: 'animated slideInRight',
        close: 'animated slideOutRight'
      }

    });
        noty.show();
    },

    ajax_req: function(fields, url) {
      return $.ajax({
          url:base_url+url,
          type:'POST',
          data: fields,
          datatype : "application/json"
      });
    },

    _addpages:function(){
        $("#pages_form").validate({
       onkeyup: false,
       onclick: false,
       onfocusout: false,
            rules: {
                title: {
                    required: true, 
                },   
                content:
                {
                    required: true,
                }
            },
            messages: {
                title: {
                  required: "Please Enter Category",
                },
                content:
                {
                  required: "Please Enter content",
                }

            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                error.insertAfter( element );
                
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
                var hpid = $("#hpid").val();
                if(hpid)
                {
                     pages.ajax_req(($("#pages_form").serialize()), 'updatepages').done(function (response) {
                    var res = $.parseJSON(response);
                    if(res.type == 'success'){
                        pages.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                    }else if(res.type == 'warning'){
                        pages.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;;
                    }else{
                        pages.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                    }
                });
                return false;
                }
                else
                {
                   
                    pages.ajax_req(($("#pages_form").serialize()), 'addpages').done(function (response) {
                    var res = $.parseJSON(response);
                    if(res.type == 'success'){
                        pages.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                    }else if(res.type == 'warning'){
                        pages.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;;
                    }else{
                        pages.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                    }
                });
                return false;
                }
            }
        })
    },
}
var base_url = $("#myBase").attr('href');

$(function() {
    subcate.init();
    
}); 


subcate = {
  init:function () {
    subcate._extrafunction();
    subcate._subcategory();

  },
    _extrafunction:function(){
    $(document).ready(function () {
        bsCustomFileInput.init();
        $('#delete_all').click(function(){
      var checkbox = $('.delete_checkbox:checked');
  
      if(checkbox.length > 0)
      {
       var checkbox_value = [];
       $(checkbox).each(function(){
        checkbox_value.push($(this).val());
       });
     
       Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!',
              showLoaderOnConfirm: true,
              preConfirm: () => {
                    $.ajax({
                            type: 'POST',
                            url: base_url+'delsubcategory',
                            data:{id:checkbox_value},
                            success: function(response){
                                var temp = JSON.parse(response);
                                    if(temp.type == 'success'){
                                        Swal.fire({
                                                title: temp.msg,
                                                type: "success",
                                                confirmButtonClass: 'btn-primary btn-md waves-effect waves-light',
                                                confirmButtonText: 'Ok',
                                                closeOnConfirm: false,
                                                onAfterClose:()=>{
                                                  redirecturl(temp.url)
                                                }
                                        })
                                    }else {
                                        Swal.fire("No records found", '', "error");
                                    }

                            }

                        });
              }
        }) ;
   function  redirecturl(url =""){
                 window.location.href = url;
              }

  }
  else
  {
   alert('Select atleast one records');
  }

 });
   });
},


  notifyWithtEle: function (msg,type,pos,timeout) {
        pos = "";
    timeout = "";
        var noty = new Noty({
      theme:'metroui',
      text: msg,
      type: type,
      layout: (pos != "") ? pos : 'topRight',
      timeout: (timeout != "") ? timeout : 2000,
      closeWith: ['click'],
      animation: {
        open: 'animated slideInRight',
        close: 'animated slideOutRight'
      }

    });
        noty.show();
    },

    ajax_req: function(fields, url) {
      return $.ajax({
          url:base_url+url,
          type:'POST',
          data: fields,
          datatype : "application/json"
      });
    },

    _subcategory:function(){

         $.validator.addMethod("validatesubcname", function(value, element, jdata) {
        var x = $.ajax({
            type: "POST",
            url: base_url + "verifysubcategory",
            async: false,
            dataType: "json",
            data: jdata,
        }).responseText;
    /* setTimeout(function(){
              return (x === 'false') ? false : true;
           }, 3000);*/
           //alert(x);return false;
     return (x === 'false') ? false : true;
     }, "Category name is already available.");


$.validator.addMethod('filesize', function (value, element, param) {
  return this.optional(element) || (element.files[0].size <= param)
}, 'File size must be less than {10} bytes.');
   
        $("#subcategory_form").validate({
        onkeyup: false,
       onclick: false,
       onfocusout: false,
            rules: {
                subcatname: {
                    required: true,   
                    validatesubcname: {
                    subcatname: function(){
                        return $("#subcatname").val();
                    },
                    _subcatname: function(){
                        return $("#_subcatname").val();
                    }
                }
                },   
                parent_id:
                {
                    required: true,
                },

                subcatimage:
                {
                    filesize:10000000
                }
            },
            messages: {
                subcatname: {
                  required: "Please Enter Category",
                },
                parent_id:{
                  required: "Please Select Parent Category"
                },

            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                error.insertAfter( element );
                
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
                var hsubcatid = $("#hsubcatid").val();
                var file_data = $('#subcatimage').prop('files')[0];
                var form_data = new FormData($('#subcategory_form')[0]);
                form_data.append('file', file_data);
                if(hsubcatid)
                {
                    $.ajax({
                            url: base_url + 'updatesubcategory', // point to server-side controller method
                           datatype : "application/json", // what to expect back from the server
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                 var res = $.parseJSON(response);
                                if(res.type == 'success'){
                            subcate.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            subcate.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            subcate.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                            },
                            
                        });
                    return false;
                }
                else
                {
                    
                    $.ajax({
                            url: base_url + 'addsubcategory', // point to server-side controller method
                            datatype : "application/json", // what to expect back from the server
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                                 var res = $.parseJSON(response);
                                if(res.type == 'success'){
                            subcate.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else if(res.type == 'warning'){
                            subcate.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            window.location = base_url+res.url;
                        }else{
                            subcate.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        }
                            },
                            
                        });
                    return false;
                }
            }
        })

    },

}
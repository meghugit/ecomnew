var base_url = $("#myBase").attr('href');

$(function() {
    store.init();
    
}); 


store = {
  init:function () {
    store._extrafunction();
    store._storecode();
  },
    _extrafunction:function(){
    
    $(document).ready(function () {

        bsCustomFileInput.init();

    })
    
     /*   
    store.ajax_req({''})*/
    $(document).on('click','.undo',function(e) {
       $(this).prev().remove();
    $(this).remove();
    var id = $(this).data('param');
      store.ajax_req({id:id},'dellogo').done(function (response) {
                    /*var res = $.parseJSON(response);
                    if(res.type == 'success'){
                      store.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                        window.location = base_url+res.url;
                    }*/
          });
          return false;

   
            
});
},

  notifyWithtEle: function (msg,type,pos,timeout) {
        pos = "";
    timeout = "";
        var noty = new Noty({
      theme:'metroui',
      text: msg,
      type: type,
      layout: (pos != "") ? pos : 'topRight',
      timeout: (timeout != "") ? timeout : 2000,
      closeWith: ['click'],
      animation: {
        open: 'animated slideInRight',
        close: 'animated slideOutRight'
      }

    });
        noty.show();
    },

    ajax_req: function(fields, url) {
      return $.ajax({
          url:base_url+url,
          type:'POST',
          data: fields,
          datatype : "application/json"
      });
    },

   
    _storecode : function () {
  
        $("#store_form").validate({
            rules: {
                stname: {
                    required: true,   
                },   
                des: {
                    required:true,
                },
                email:{
                    required:true,
                },
                mno :{
                    required:true,
                    digits:true,

                },
                add: {
                    required :true,
                },
                
            },
            messages: {
                stname: {
                  required: "Please Enter Name",
                },
                des: {
                    required :"Please Enter Description",
                },
                email:{
                    required:"Please Enter Email",
                },
                mno: {
                    required:"Please Enter Mobile no",
                    digits:"Please Enter Only Number",
                },
                add:{
                    required :"Please Enter Address",
                },
                
               
            },
            errorElement: "em",
            errorPlacement: function ( error, element ) {
                error.addClass( "help-block" );
                console.log(element);

                if(element.prop( "class" ) === "form-control datetimepicker-input"){
                  error.insertAfter( element.parent( "div" ) );
                } 
                else {
                  error.insertAfter( element );
                }
            },
            highlight: function ( element, errorClass, validClass ) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-error" ).removeClass( "has-success" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $( element ).parents( ".padding-leftright-null" ).addClass( "has-success" ).removeClass( "has-error" );
            },
         submitHandler: function (form) {
                var file_data = $('#storeimag').prop('files')[0];
                var form_data = new FormData($('#store_form')[0]);
                form_data.append('file', file_data);
                var hsid = $("#hsid").val();
                if(hsid)
                {
                    $.ajax({
                            url: base_url + 'updatestore', // point to server-side controller method
                           datatype : "application/json", // what to expect back from the server
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                            var res = $.parseJSON(response);
                            if(res.type == 'success'){
                                store.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                window.location = base_url+res.url;
                            }else if(res.type == 'warning'){
                                store.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                window.location = base_url+res.url;
                            }else{
                                store.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            }
                            },
                            
                        });
                    return false;
                }
                else
                {
                    $.ajax({
                            url: base_url + 'addstore', // point to server-side controller method
                           datatype : "application/json", // what to expect back from the server
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function (response) {
                            var res = $.parseJSON(response);
                            if(res.type == 'success'){
                                store.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                window.location = base_url+res.url;
                            }else if(res.type == 'warning'){
                                store.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                                window.location = base_url+res.url;
                            }else{
                                store.notifyWithtEle(res.msg , res.type ,'topRight', 2000);
                            }
                            },
                            
                        });
                    return false;
                }
            }
        })
    },
   
}